"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_people admin *

:details: lara_django_people admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_people > forms.py" to update this file
________________________________________________________________________
"""
# django crispy forms s. https://github.com/django-crispy-forms/django-crispy-forms for []
# generated with django-extensions forms_generator -c  [] > forms.py (LARA-version)

from django import forms
from django.forms.widgets import DateInput, DateTimeInput
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Fieldset, Layout, Submit, Row, Column

from .models import (
    ExtraData,
    EntityClass,
    EntityTitle,
    Entity,
    EntityRole,
    EntityBankAccount,
    LaraUser,
    MeetingsCalendar,
)


class ExtraDataCreateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
            "data_type",
            "name",
            "name_full",
            "title",
            "namespace",
            "version",
            "UUID",
            "hash_SHA256",
            "data_JSON",
            "media_type",
            "IRI",
            "URL",
            "description",
            "file",
            "image",
            "office_room",
            "laboratory",
            "email",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "data_type",
            "name",
            "name_full",
            "title",
            "namespace",
            "version",
            "UUID",
            "hash_SHA256",
            "data_JSON",
            "media_type",
            "IRI",
            "URL",
            "description",
            "file",
            "image",
            "office_room",
            "laboratory",
            "email",
            Submit("submit", "Create"),
        )


class ExtraDataUpdateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
            "data_type",
            "name",
            "name_full",
            "title",
            "namespace",
            "version",
            "UUID",
            "hash_SHA256",
            "data_JSON",
            "media_type",
            "IRI",
            "URL",
            "description",
            "file",
            "image",
            "office_room",
            "laboratory",
            "email",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "data_type",
            "name",
            "name_full",
            "title",
            "namespace",
            "version",
            "UUID",
            "hash_SHA256",
            "data_JSON",
            "media_type",
            "IRI",
            "URL",
            "description",
            "file",
            "image",
            "office_room",
            "laboratory",
            "email",
            Submit("submit", "Create"),
        )


class EntityClassCreateForm(forms.ModelForm):
    class Meta:
        model = EntityClass
        fields = ("name", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("name", "description", Submit("submit", "Create"))


class EntityClassUpdateForm(forms.ModelForm):
    class Meta:
        model = EntityClass
        fields = ("name", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("name", "description", Submit("submit", "Create"))


class EntityRoleCreateForm(forms.ModelForm):
    class Meta:
        model = EntityRole
        fields = ("name", "IRI", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("name", "IRI", "description", Submit("submit", "Create"))


class EntityRoleUpdateForm(forms.ModelForm):
    class Meta:
        model = EntityRole
        fields = ("name", "IRI", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("name", "IRI", "description", Submit("submit", "Create"))


class EntityTitleCreateForm(forms.ModelForm):
    class Meta:
        model = EntityTitle
        fields = ("title", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("title", "description", Submit("submit", "Create"))


class EntityTitleUpdateForm(forms.ModelForm):
    class Meta:
        model = EntityTitle
        fields = ("title", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("title", "description", Submit("submit", "Create"))


class EntityCreateForm(forms.ModelForm):
    # affiliation_start= forms.DateTimeField(widget=DateTimeInput(), label="Affl. Start", required=False)
    class Meta:
        model = Entity
        fields = (
            "slug",
            "entity_class",
            "gender",
            "title",
            "name_first",
            "names_middle",
            "name_last",
            "name_full",
            "names_last_previous",
            "name_nick",
            "acronym",
            "role_current",
            "URL",
            "PID",
            "ORCID",
            "ROR",
            "TAX_no",
            "VAT_no",
            "TOLL_no",
            "affiliation_current",
            "affiliation_start",
            "affiliation_end",
            "office_room",
            "laboratory",
            "email",
            "email_private",
            "email_permanent",
            "website",
            "phone_number_mobile",
            "phone_number_office",
            "phone_number_lab",
            "phone_number_home",
            "address",
            "date_birth",
            "date_death",
            "color",
            "icon",
            "image",
            "IRI",
        )
        widgets = {
            "name_first": forms.TextInput(attrs={"placeholder": "First name", "required": True, "rows": 1}),
            "names_middle": forms.TextInput(attrs={"placeholder": "Middle names", "required": False, "rows": 1}),
            "name_last": forms.TextInput(attrs={"placeholder": "Last name", "required": False, "rows": 1}),
            "name_full": forms.TextInput(attrs={"placeholder": "Full name", "required": False, "rows": 1}),
            "names_last_previous": forms.TextInput(
                attrs={"placeholder": "Previous last names", "required": False, "rows": 1}
            ),
            "name_nick": forms.TextInput(attrs={"placeholder": "Nick name", "required": False, "rows": 1}),
            "acronym": forms.TextInput(attrs={"placeholder": "Acronym", "required": False, "rows": 1}),
            "URL": forms.URLInput(),
            "PID": forms.URLInput(),
            "ORCID": forms.TextInput(attrs={"placeholder": "ORCID", "required": False, "rows": 1}),
            "ROR": forms.TextInput(attrs={"placeholder": "ROR", "required": False, "rows": 1}),
            "TAX_no": forms.TextInput(attrs={"placeholder": "TAX no", "required": False, "rows": 1}),
            "VAT_no": forms.TextInput(attrs={"placeholder": "VAT no", "required": False, "rows": 1}),
            "TOLL_no": forms.TextInput(attrs={"placeholder": "TOLL no", "required": False, "rows": 1}),
            "email": forms.EmailInput(),
            "email_private": forms.EmailInput(),
            "email_permanent": forms.EmailInput(),
            "website": forms.URLInput(),
            "phone_number_mobile": forms.TextInput(
                attrs={"placeholder": "Mobile phone number", "required": False, "rows": 1}
            ),
            "phone_number_office": forms.TextInput(
                attrs={"placeholder": "Office phone number", "required": False, "rows": 1}
            ),
            "phone_number_lab": forms.TextInput(
                attrs={"placeholder": "Lab phone number", "required": False, "rows": 1}
            ),
            "phone_number_home": forms.TextInput(
                attrs={"placeholder": "Home phone number", "required": False, "rows": 1}
            ),
            "affiliation_start": DateInput(attrs={"type": "date"}),
            "affiliation_end": DateInput(attrs={"type": "date"}),
            "date_birth": DateInput(attrs={"type": "date"}),
            "date_death": DateInput(attrs={"type": "date"}),
            "icon": forms.TextInput(attrs={"type": "file"}),
            "color": forms.TextInput(attrs={"type": "color"}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("entity_class", css_class="form-group col-md-3 mb-0"),
                Column("gender", css_class="form-group col-md-2 mb-0"),
                Column("title", css_class="form-group col-md-2 mb-0"),
                Column("role_current", css_class="form-group col-md-2 mb-0"),
            ),
            Row(
                Column("name_first", css_class="form-group col-md-4 mb-0", input_size="input-group-sm"),
                Column("names_middle", css_class="form-group col-md-4 mb-0"),
                Column("name_last", css_class="form-group col-md-4 mb-0"),
                # limit row height to 1 line  - currently not working
                # input_size="input-group-sm"
            ),
            Row(
                Column("name_full", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("name_nick", css_class="form-group col-md-4 mb-0"),
                Column("acronym", css_class="form-group col-md-4 mb-0"),
                Column("names_last_previous", css_class="form-group col-md-4 mb-0"),
            ),
            Row(
                Column("email", css_class="form-group col-md-4 mb-0"),
                Column("email_private", css_class="form-group col-md-4 mb-0"),
                Column("email_permanent", css_class="form-group col-md-4 mb-0"),
            ),
            Row(
                Column("website", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("phone_number_mobile", css_class="form-group col-md-6 mb-0"),
                Column("phone_number_office", css_class="form-group col-md-6 mb-0"),
                Column("phone_number_lab", css_class="form-group col-md-6 mb-0"),
                Column("phone_number_home", css_class="form-group col-md-6 mb-0"),
                Column("address", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("ORCID", css_class="form-group col-md-6 mb-0"),
                Column("ROR", css_class="form-group col-md-6 mb-0"),
                Column("URL", css_class="form-group col-md-6 mb-0"),
                Column("PID", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("TAX_no", css_class="form-group col-md-4 mb-0"),
                Column("VAT_no", css_class="form-group col-md-4 mb-0"),
                Column("TOLL_no", css_class="form-group col-md-4 mb-0"),
            ),
            Row(
                Column("affiliation_current", css_class="form-group col-md-4 mb-0"),
                Column("affiliation_start", css_class="form-group col-md-4 mb-0"),
                Column("affiliation_end", css_class="form-group col-md-4 mb-0"),
            ),
            Row(
                Column("office_room", css_class="form-group col-md-6 mb-0"),
                Column("laboratory", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("date_birth", css_class="form-group col-md-6 mb-0"),
                Column("date_death", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("color", css_class="form-group col-md-3 mb-0"),
                Column("icon", css_class="form-group col-md-3 mb-0"),
                Column("image", css_class="form-group col-md-3 mb-0"),
            ),
            Submit("submit", "Create"),
        )


class EntityUpdateForm(forms.ModelForm):
    class Meta:
        model = Entity
        fields = (
            "slug",
            "entity_class",
            "gender",
            "title",
            "name_first",
            "names_middle",
            "name_last",
            "name_full",
            "names_last_previous",
            "name_nick",
            "acronym",
            "role_current",
            "URL",
            "PID",
            "IRI",
            "ORCID",
            "ROR",
            "TAX_no",
            "VAT_no",
            "TOLL_no",
            "affiliation_current",
            "affiliation_start",
            "affiliation_end",
            "office_room",
            "laboratory",
            "email",
            "email_private",
            "email_permanent",
            "website",
            "phone_number_mobile",
            "phone_number_office",
            "phone_number_lab",
            "phone_number_home",
            "address",
            "date_birth",
            "date_death",
            "color",
            "icon",
            "image",
        )
        widgets = {
            "name_first": forms.TextInput(attrs={"placeholder": "First name", "required": True, "rows": 1}),
            "names_middle": forms.TextInput(attrs={"placeholder": "Middle names", "required": False, "rows": 1}),
            "name_last": forms.TextInput(attrs={"placeholder": "Last name", "required": False, "rows": 1}),
            "name_full": forms.TextInput(attrs={"placeholder": "Full name", "required": False, "rows": 1}),
            "names_last_previous": forms.TextInput(
                attrs={"placeholder": "Previous last names", "required": False, "rows": 1}
            ),
            "name_nick": forms.TextInput(attrs={"placeholder": "Nick name", "required": False, "rows": 1}),
            "acronym": forms.TextInput(attrs={"placeholder": "Acronym", "required": False, "rows": 1}),
            "URL": forms.URLInput(),
            "PID": forms.URLInput(),
            "ORCID": forms.TextInput(attrs={"placeholder": "ORCID", "required": False, "rows": 1}),
            "ROR": forms.TextInput(attrs={"placeholder": "ROR", "required": False, "rows": 1}),
            "TAX_no": forms.TextInput(attrs={"placeholder": "TAX no", "required": False, "rows": 1}),
            "VAT_no": forms.TextInput(attrs={"placeholder": "VAT no", "required": False, "rows": 1}),
            "TOLL_no": forms.TextInput(attrs={"placeholder": "TOLL no", "required": False, "rows": 1}),
            "email": forms.EmailInput(),
            "email_private": forms.EmailInput(),
            "email_permanent": forms.EmailInput(),
            "website": forms.URLInput(),
            "phone_number_mobile": forms.TextInput(
                attrs={"placeholder": "Mobile phone number", "required": False, "rows": 1}
            ),
            "phone_number_office": forms.TextInput(
                attrs={"placeholder": "Office phone number", "required": False, "rows": 1}
            ),
            "phone_number_lab": forms.TextInput(
                attrs={"placeholder": "Lab phone number", "required": False, "rows": 1}
            ),
            "phone_number_home": forms.TextInput(
                attrs={"placeholder": "Home phone number", "required": False, "rows": 1}
            ),
            "affiliation_start": DateInput(attrs={"type": "date"}),
            "affiliation_end": DateInput(attrs={"type": "date"}),
            "date_birth": DateInput(attrs={"type": "date"}),
            "date_death": DateInput(attrs={"type": "date"}),
            "icon": forms.TextInput(attrs={"type": "file"}),
            "color": forms.TextInput(attrs={"type": "color"}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("entity_class", css_class="form-group col-md-3 mb-0"),
                Column("gender", css_class="form-group col-md-2 mb-0"),
                Column("title", css_class="form-group col-md-2 mb-0"),
                Column("role_current", css_class="form-group col-md-2 mb-0"),
            ),
            Row(
                Column("name_first", css_class="form-group col-md-4 mb-0", input_size="input-group-sm"),
                Column("names_middle", css_class="form-group col-md-4 mb-0"),
                Column("name_last", css_class="form-group col-md-4 mb-0"),
                # limit row height to 1 line  - currently not working
                # input_size="input-group-sm"
            ),
            Row(
                Column("name_full", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("name_nick", css_class="form-group col-md-4 mb-0"),
                Column("acronym", css_class="form-group col-md-4 mb-0"),
                Column("names_last_previous", css_class="form-group col-md-4 mb-0"),
            ),
            Row(
                Column("email", css_class="form-group col-md-4 mb-0"),
                Column("email_private", css_class="form-group col-md-4 mb-0"),
                Column("email_permanent", css_class="form-group col-md-4 mb-0"),
            ),
            Row(
                Column("website", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("phone_number_mobile", css_class="form-group col-md-6 mb-0"),
                Column("phone_number_office", css_class="form-group col-md-6 mb-0"),
                Column("phone_number_lab", css_class="form-group col-md-6 mb-0"),
                Column("phone_number_home", css_class="form-group col-md-6 mb-0"),
                Column("address", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("ORCID", css_class="form-group col-md-6 mb-0"),
                Column("ROR", css_class="form-group col-md-6 mb-0"),
                Column("URL", css_class="form-group col-md-6 mb-0"),
                Column("PID", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("TAX_no", css_class="form-group col-md-4 mb-0"),
                Column("VAT_no", css_class="form-group col-md-4 mb-0"),
                Column("TOLL_no", css_class="form-group col-md-4 mb-0"),
            ),
            Row(
                Column("affiliation_current", css_class="form-group col-md-4 mb-0"),
                Column("affiliation_start", css_class="form-group col-md-4 mb-0"),
                Column("affiliation_end", css_class="form-group col-md-4 mb-0"),
            ),
            Row(
                Column("office_room", css_class="form-group col-md-6 mb-0"),
                Column("laboratory", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("date_birth", css_class="form-group col-md-6 mb-0"),
                Column("date_death", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("color", css_class="form-group col-md-3 mb-0"),
                Column("icon", css_class="form-group col-md-3 mb-0"),
                Column("image", css_class="form-group col-md-3 mb-0"),
            ),
            Submit("submit", "Update"),
        )


class EntityBankAccountCreateForm(forms.ModelForm):
    class Meta:
        model = EntityBankAccount
        fields = ("account_name", "account_no", "IBAN", "BIC", "bank", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "account_name", "account_no", "IBAN", "BIC", "bank", "description", Submit("submit", "Create")
        )


class EntityBankAccountUpdateForm(forms.ModelForm):
    class Meta:
        model = EntityBankAccount
        fields = ("account_name", "account_no", "IBAN", "BIC", "bank", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "account_name", "account_no", "IBAN", "BIC", "bank", "description", Submit("submit", "Update")
        )


class LaraUserCreateForm(forms.ModelForm):
    class Meta:
        model = LaraUser
        fields = (
            "entity",
            "user",
            "home_screen_layout",
            "access_TOKEN",
            "access_control",
            "confirmation_TOKEN",
            "email_confirmed",
            "email_recover",
            "max_logins",
            "failed_logins",
            "IP_curr_login",
            "IP_last_login",
            "datetime_confirmed",
            "datetime_confirmation_sent",
        )
        widgets = {
            "entity": forms.TextInput(attrs={"placeholder": "Entity name"}),
            "user": forms.TextInput(attrs={"placeholder": "Username"}),
            "access_TOKEN": forms.PasswordInput(attrs={"placeholder": "Access token"}),
            "confirmation_TOKEN": forms.PasswordInput(attrs={"placeholder": "Confirmation token"}),
            "email_confirmed": forms.EmailInput(attrs={"placeholder": "Confirmed email"}),
            "email_recover": forms.EmailInput(attrs={"placeholder": "Recovery email"}),
            "IP_curr_login": forms.TextInput(attrs={"placeholder": "Current IP address"}),
            "IP_last_login": forms.TextInput(attrs={"placeholder": "Last IP address"}),
            "datetime_confirmed": DateTimeInput(attrs={"type": "date"}),
            "datetime_confirmation_sent": DateTimeInput(attrs={"type": "date"}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                "Create Lara User",
                Row(
                    Column("entity", css_class="form-group col-md-6 mb-0"),
                    Column("user", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column("access_TOKEN", css_class="form-group col-md-6 mb-0"),
                    Column("confirmation_TOKEN", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column("email_confirmed", css_class="form-group col-md-6 mb-0"),
                    Column("email_recover", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column("max_logins", css_class="form-group col-md-4 mb-0"),
                    Column("failed_logins", css_class="form-group col-md-4 mb-0"),
                    Column("datetime_confirmed", css_class="form-group col-md-4 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column("IP_curr_login", css_class="form-group col-md-6 mb-0"),
                    Column("IP_last_login", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                Row(Column("datetime_confirmation_sent", css_class="form-group col-md-6 mb-0"), css_class="form-row"),
                Submit("submit", "Create", css_class="btn-primary"),
            )
        )


class LaraUserUpdateForm(forms.ModelForm):
    class Meta:
        model = LaraUser
        fields = (
            "entity",
            "user",
            "home_screen_layout",
            "access_TOKEN",
            "access_control",
            "confirmation_TOKEN",
            "email_confirmed",
            "email_recover",
            "max_logins",
            "failed_logins",
            "IP_curr_login",
            "IP_last_login",
            "datetime_confirmed",
            "datetime_confirmation_sent",
        )
        widgets = {
            "entity": forms.TextInput(attrs={"placeholder": "Entity name"}),
            "user": forms.TextInput(attrs={"placeholder": "Username"}),
            "access_TOKEN": forms.PasswordInput(attrs={"placeholder": "Access token"}),
            "confirmation_TOKEN": forms.PasswordInput(attrs={"placeholder": "Confirmation token"}),
            "email_confirmed": forms.EmailInput(attrs={"placeholder": "Confirmed email"}),
            "email_recover": forms.EmailInput(attrs={"placeholder": "Recovery email"}),
            "IP_curr_login": forms.TextInput(attrs={"placeholder": "Current IP address"}),
            "IP_last_login": forms.TextInput(attrs={"placeholder": "Last IP address"}),
            "datetime_confirmed": DateTimeInput(attrs={"type": "date"}),
            "datetime_confirmation_sent": DateTimeInput(attrs={"type": "date"}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                "Update Lara User",
                Row(
                    Column("entity", css_class="form-group col-md-6 mb-0"),
                    Column("user", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column("access_TOKEN", css_class="form-group col-md-6 mb-0"),
                    Column("confirmation_TOKEN", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column("email_confirmed", css_class="form-group col-md-6 mb-0"),
                    Column("email_recover", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column("max_logins", css_class="form-group col-md-4 mb-0"),
                    Column("failed_logins", css_class="form-group col-md-4 mb-0"),
                    Column("datetime_confirmed", css_class="form-group col-md-4 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column("IP_curr_login", css_class="form-group col-md-6 mb-0"),
                    Column("IP_last_login", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                Row(Column("datetime_confirmation_sent", css_class="form-group col-md-6 mb-0"), css_class="form-row"),
                Submit("submit", "Update", css_class="btn-primary"),
            )
        )


class MeetingsCalendarCreateForm(forms.ModelForm):
    class Meta:
        model = MeetingsCalendar
        fields = (
            "title",
            "calendar_URL",
            "summary",
            "description",
            "color",
            "ics",
            "datetime_start",
            "datetime_end",
            "duration",
            "all_day",
            "created",
            "datetime_last_modified",
            "timestamp",
            "location",
            "geolocation",
            "conference_URL",
            "range",
            "related",
            "role",
            "tzid",
            "offset_UTC",
            "alarm_repeat_JSON",
            "event_repeat",
            "event_repeat_JSON",
            "organiser",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "title",
            "calendar_URL",
            "summary",
            "description",
            "color",
            "ics",
            "datetime_start",
            "datetime_end",
            "duration",
            "all_day",
            "created",
            "datetime_last_modified",
            "timestamp",
            "location",
            "geolocation",
            "conference_URL",
            "range",
            "related",
            "role",
            "tzid",
            "offset_UTC",
            "alarm_repeat_JSON",
            "event_repeat",
            "event_repeat_JSON",
            "organiser",
            Submit("submit", "Create"),
        )


class MeetingsCalendarUpdateForm(forms.ModelForm):
    class Meta:
        model = MeetingsCalendar
        fields = (
            "title",
            "calendar_URL",
            "summary",
            "description",
            "color",
            "ics",
            "datetime_start",
            "datetime_end",
            "duration",
            "all_day",
            "created",
            "datetime_last_modified",
            "timestamp",
            "location",
            "geolocation",
            "conference_URL",
            "range",
            "related",
            "role",
            "tzid",
            "offset_UTC",
            "alarm_repeat_JSON",
            "event_repeat",
            "event_repeat_JSON",
            "organiser",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "title",
            "calendar_URL",
            "summary",
            "description",
            "color",
            "ics",
            "datetime_start",
            "datetime_end",
            "duration",
            "all_day",
            "created",
            "datetime_last_modified",
            "timestamp",
            "location",
            "geolocation",
            "conference_URL",
            "range",
            "related",
            "role",
            "tzid",
            "offset_UTC",
            "alarm_repeat_JSON",
            "event_repeat",
            "event_repeat_JSON",
            "organiser",
            Submit("submit", "Update"),
        )


# from .forms import ExtraDataCreateForm, EntityClassCreateForm, EntityTitleCreateForm, EntityCreateForm, EntityBankAccountCreateForm, LaraUserCreateForm, MeetingsCalendarCreateFormExtraDataUpdateForm, EntityClassUpdateForm, EntityTitleUpdateForm, EntityUpdateForm, EntityBankAccountUpdateForm, LaraUserUpdateForm, MeetingsCalendarUpdateForm
