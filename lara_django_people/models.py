"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_people models *

:details: lara_django_people database models.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""


import logging
import datetime
import uuid
import hashlib
import json
from random import randint

from django.utils import timezone
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.db import models
from django.contrib.auth.models import User

from lara_django_base.lara_color_schemes import ColorBlind16
from lara_django_base.models import (
    Namespace,
    ExtraDataAbstr,
    Tag,
    Room,
    Address,
    CalendarAbstract,
)

settings.FIXTURES += ["2_entitiy_roles_fix", "4_entity_class_fix", "6_entity_title_fix"]


class ExtraData(ExtraDataAbstr):
    """
    This class can be used to extend data, by extra information,
    e.g., more telephone numbers, customer numbers, ...
    """

    model_CURI = "lara:people/ExtraData"
    extradata_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    file = models.FileField(upload_to="people", blank=True, null=True, help_text="rel. path/filename")
    image = models.ImageField(
        upload_to="people/images/",
        blank=True,
        default="image.svg",
        help_text="location room map rel. path/filename to image",
    )
    office_room = models.ForeignKey(
        Room,
        related_name="%(app_label)s_%(class)s_office_room_related",
        related_query_name="%(app_label)s_%(class)s_office_room_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="extra office",
    )
    laboratory = models.ForeignKey(
        Room,
        related_name="%(app_label)s_%(class)s_laboratory_related",
        related_query_name="%(app_label)s_%(class)s_laboratory_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="extra laboratory",
    )
    email = models.EmailField(blank=True, help_text="extra email")

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for name_full
        """
        if self.hash_SHA256 is None:
            if self.data_JSON is not None:
                to_hash = json.dumps(self.data_JSON)  # +  str(self.UUID) +
                self.hash_SHA256 = hashlib.sha256(to_hash.encode("utf-8")).hexdigest()
            else:
                self.hash_SHA256 = hashlib.sha256(str(self.UUID).encode("utf-8")).hexdigest()

        if self.name is None or self.name == "":
            if self.title is None or self.title == "":
                self.name = "_".join(
                    (
                        timezone.now().strftime("%Y%m%d_%H%M%S"),
                        "data",
                        self.hash_SHA256[:8],
                    )
                )
            else:
                self.name = self.title.replace(" ", "_").lower().strip()

        else:
            self.name = self.name.replace(" ", "_").lower().strip()
        if self.name_full is None or self.name_full == "":
            if self.namespace is not None and self.version is not None:
                self.name_full = f"{self.namespace.URI}/" + "_".join((self.name.replace(" ", "_"), self.version))
            else:
                self.name_full = "_".join((self.name.replace(" ", "_"), self.version))

        if not self.IRI:
            netloc = self.namespace.parse_URI().netloc
            path = self.namespace.parse_URI().path

            self.IRI = f"{settings.LARA_PREFIXES['lara']}people/ExtraData/{netloc}{path}/{self.name.replace(' ', '_').lower().strip()}"

        super().save(*args, **kwargs)


class EntityClass(models.Model):
    """
    Entity class, like university, institute, company, organisation,
    but also guest_scientist, bachelor student, master, postdoc, group leader.
    """

    model_CURI = "lara:people/EntityClass"
    entity_class_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(unique=True, help_text="entity class, like university, institute, company, organisation")
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(blank=True, help_text="description of the entity class")

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for IRI
        """
        if self.IRI is None or self.IRI == "":
            self.IRI = (
                f"{settings.LARA_PREFIXES['lara']}people/EntityClass/{self.name.replace(' ', '_').lower().strip()}"
            )

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name or ""

    def __repr__(self):
        return self.name or ""

    class Meta:
        verbose_name_plural = "EntityClasses"


class EntityRole(models.Model):
    """
    Entity role, like scientist, reviewer, developer, maintainer, student, postdoc, project leader, ...
    """

    model_CURI = "lara:people/EntityRole"
    role_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(
        unique=True, help_text="entity role, like guest_scientist,  bachelor student, master, postdoc, ..."
    )
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        max_length=512,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(blank=True, help_text="description of the entity role")

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for IRI
        """
        if self.IRI is None or self.IRI == "":
            self.IRI = (
                f"{settings.LARA_PREFIXES['lara']}people/EntityRole/{self.name.replace(' ', '_').lower().strip()}"
            )

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name or ""

    class Meta:
        verbose_name_plural = "EntityRoles"


class EntityTitle(models.Model):
    """
    Entity title, like academic title, e.g., Dr., Prof., Dr. ...
    """

    model_CURI = "lara:people/EntityTitle"
    entity_title_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.TextField(
        blank=True, null=True, unique=True, help_text="entity title, like academic title, e.g. Dr., Prof., Dr. ... "
    )
    description = models.TextField(blank=True, help_text="description of the entity title")

    def __str__(self):
        return self.title or ""


class Entity(models.Model):
    """
    Generic Entity Model. An Entity could be, e.g., a person or institution or company.
    The term "entity" encounters for this generalisation.
    """

    class GenderChoices(models.TextChoices):
        GENDER_MALE = "m", _("male")
        GENDER_FEMALE = "f", _("female")
        GENDER_NOT_SPECIFIED = "n", _("not specified")

    model_CURI = "lara:people/Entity"
    entity_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    slug = models.SlugField(
        max_length=256,
        blank=True,
        null=True,
        unique=True,
        help_text="slugs are used to display the entity in, e.g. URIs, auto generated, do not use any special characters",
    )  # unique generation does not work with fixtures !
    entity_class = models.ForeignKey(
        EntityClass,
        related_name="%(app_label)s_%(class)s_names_related",
        related_query_name="%(app_label)s_%(class)s_names_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="class/type/position of the entity, e.g., university, institute, company",
    )
    gender = models.CharField(
        max_length=1,
        choices=GenderChoices.choices,
        default=GenderChoices.GENDER_NOT_SPECIFIED,
    )
    title = models.ForeignKey(
        EntityTitle,
        related_name="%(app_label)s_%(class)s_titles_related",
        related_query_name="%(app_label)s_%(class)s_titles_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="entity title, e.g. Dr., Prof. Dr. ",
    )
    name_first = models.TextField(blank=True, help_text="first name of a person or name of an institution/company")
    names_middle = models.TextField(
        blank=True,
        null=True,
        help_text="list of middle names separated by space, e.g. Alfred Ernst Walter Neumann -> Ernst Walter",
    )
    name_last = models.TextField(
        null=True,
        blank=True,
        help_text="last name, e.g. vonSydow - aristocratic prepositions are part of the last name",
    )
    name_full = models.TextField(
        blank=True,
        help_text="full name, e.g. University of Greifswald, Institute for Biochemistry, Max vonSydow, is auto-generated when saved",
    )
    names_last_previous = models.TextField(
        blank=True,
        null=True,
        help_text="list of previous last names separated by space, e.g. changed after marriage, order: latest -> earliest",
    )
    name_nick = models.TextField(blank=True, null=True, help_text="the entities nickname, e.g., James -> Jim.")
    acronym = models.TextField(
        blank=True,
        null=True,
        help_text="3 letter code, autogenerated, should be unique with an institution, could also be used for company acronyms",
    )
    # "uniquness should be only within one institution"
    URL = models.URLField(blank=True, null=True, help_text="Universal Resource Locator - URL, web address of entity")
    PID = models.URLField(blank=True, null=True, unique=True, help_text="handle URI")
    IRI = models.URLField(
        blank=True,
        null=True,
        unique=True,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    ORCID = models.TextField(
        blank=True,
        null=True,
        unique=True,
        help_text="ORCID is a unique researcher ID (just the ID, not the URL), s. https://orcid.org/",
    )  # if person has no ORCID, the system should point to make one
    ROR = models.TextField(
        blank=True,
        null=True,
        unique=True,
        help_text="ROR - Research Organization Registry s. https: // ror.readme.io/docs/ror-basics",
    )  # could fetch the address information automatically
    barcode = models.TextField(
        blank=True, null=True, unique=True, help_text="text representation of a barcode of the sample."
    )
    barcode_JSON = models.JSONField(
        blank=True,
        null=True,
        help_text="Advanced barcodes in JSON for multiple barcode representations, like QR codes etc.",
    )
    TAX_no = models.TextField(blank=True, null=True, help_text="Tax number")  # unique=True,
    VAT_no = models.TextField(blank=True, null=True, help_text="Value Added Tax number")  # unique=True,
    TOLL_no = models.TextField(blank=True, null=True, help_text="Toll/EORI number")  # unique=True,

    bank_accounts = models.ManyToManyField(
        "EntityBankAccount",
        blank=True,
        related_name="%(app_label)s_%(class)s_bank_accounts_related",
        related_query_name="%(app_label)s_%(class)s_bank_accounts_related_query",
        help_text="expertise/skills/profession, keywords",
    )
    expertise = models.ManyToManyField(
        Tag,
        blank=True,
        related_name="%(app_label)s_%(class)s_expertises_related",
        related_query_name="%(app_label)s_%(class)s_expertises_related_query",
        help_text="expertise/skills/profession, keywords",
    )
    interests = models.ManyToManyField(
        Tag,
        blank=True,
        related_name="%(app_label)s_%(class)s_interests_related",
        related_query_name="%(app_label)s_%(class)s_interests_related_query",
        help_text="scientific interests, keywords",
    )
    work_topics = models.ManyToManyField(
        Tag,
        blank=True,
        related_name="%(app_label)s_%(class)s_work_topics_related",
        related_query_name="%(app_label)s_%(class)s_work_topics_related_query",
        help_text="work/research topics, keywords, e.g. transaminases, electrochemistry, fuel cell development",
    )
    roles = models.ManyToManyField(
        EntityRole,
        blank=True,
        related_name="%(app_label)s_%(class)s_roles_related",
        related_query_name="%(app_label)s_%(class)s_roles_related_query",
        help_text="roles of the entity, e.g. project leader, postdoc, bachelor student, master student, guest scientist, ...",
    )
    role_current = models.ForeignKey(
        EntityRole,
        related_name="%(app_label)s_%(class)s_current_roles_related",
        related_query_name="%(app_label)s_%(class)s_current_roles_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="current role of the entity, e.g. project leader, technician postdoc, bachelor student, master student, guest scientist, ...",
    )
    affiliation_current = models.ForeignKey(
        "self",
        related_name="%(app_label)s_%(class)s_affiliations_related",
        related_query_name="%(app_label)s_%(class)s_affiliations_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="affiliation of the entity",
    )
    affiliation_start = models.DateField(
        default=datetime.date.today, help_text="start date at this affiliation, default: date of today"
    )
    affiliation_end = models.DateField(
        default=datetime.date(9999, 9, 9), help_text="end date at this affiliation, default: 9.9.9999"
    )
    affiliation_previous = models.ManyToManyField("self", blank=True, help_text="previous affiliations of the entity")
    office_room = models.ForeignKey(
        Room,
        related_name="%(app_label)s_%(class)s_office_rooms_related",
        related_query_name="%(app_label)s_%(class)s_office_rooms_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="office",
    )
    laboratory = models.ForeignKey(
        Room,
        related_name="%(app_label)s_%(class)s_laboratories_related",
        related_query_name="%(app_label)s_%(class)s_laboratories_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="laboratory",
    )
    # check relation to auth-user email !
    email = models.EmailField(blank=True, help_text="entity's current official email address")
    email_private = models.EmailField(blank=True, null=True, help_text="entity's email address")
    email_permanent = models.EmailField(blank=True, null=True, help_text="entity's permanents email address")
    website = models.URLField(blank=True, null=True, help_text="entity's website URL")
    phone_number_mobile = models.TextField(
        blank=True,
        null=True,
        help_text="entity's mobile phone number, if possible add the +Country code notation, e.g., +44 3834 420 4411",
    )
    phone_number_office = models.TextField(
        blank=True,
        null=True,
        help_text="entity's office phone number, if possible add the +Country code notation, e.g., +44 3834 420 4411",
    )
    phone_number_lab = models.TextField(
        blank=True,
        null=True,
        help_text="entity's lab phone number, if possible add the +Country code notation,e.g., +44 3834 420 4411",
    )
    phone_number_home = models.TextField(
        blank=True,
        null=True,
        help_text="entity's phone number at home, if possible add the +Country code notation,e.g., +44 3834 420 4411",
    )
    address = models.ForeignKey(
        Address,
        related_name="%(app_label)s_%(class)s_addresses_related",
        related_query_name="%(app_label)s_%(class)s_addresses_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="reference to an address",
    )
    date_birth = models.DateField(
        blank=True,
        null=True,
        help_text="date of birth of the person, could also be the founding date of an institution.",
    )
    date_death = models.DateField(
        blank=True, null=True, help_text="date of death, could also be the closing date of an institution."
    )
    color = models.TextField(
        blank=True,
        default=ColorBlind16.full_color_scheme[randint(0, 16)].get_web(),
        help_text="GUI colour representation of the entity",
    )
    icon = models.TextField(blank=True, null=True, help_text="XML/SVG icon/logo/avatar/drawing of the entity")
    image = models.ImageField(
        upload_to="people/", blank=True, default="lara.jpg", help_text="entity's image, rel. path/filename to image"
    )
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_entities_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_entities_data_extra_related_query",
        blank=True,
        help_text="e.g. ResearcherID, Scopus Author ID, extra laboratories ...",
    )
    datetime_last_modified = models.DateTimeField(
        default=timezone.now, help_text="date and time when data was last modified"
    )
    remarks = models.TextField(blank=True, null=True, help_text="remarks about the entity")

    class Meta:
        verbose_name_plural = "Entities"

    # def save(self, force_insert=None, using=None): #force_insert=force_insert, using=using
    def save(self, *args, **kwargs):
        """
        Here we generate some default values for full_name, name, acronym
        TODO: checking special characters and Capitalisation
              uniqueness of name-slug ?
        """

        entity_id = self.ORCID or self.ROR or str(self.entity_id)[:8]
        name_first = self.name_first.strip(".,-") if self.name_first else ""
        name_middle = self.names_middle.strip(".,-") if self.names_middle else ""
        name_last = self.name_last.strip(".,-") if self.name_last else ""

        if not self.name_full:
            if name_middle == "":
                self.name_full = " ".join((name_first, name_last))
            else:
                self.name_full = "_".join((name_first, name_middle, name_last))

        if not self.slug:
            self.slug = "-".join((name_first, name_last, entity_id)).lower()

        if self.ORCID == "":
            self.ORCID = None  # avoiding unique constraint violation
        if self.ROR == "":
            self.ROR = None  # avoiding unique constraint violation

        if self.barcode == "" or self.barcode is None:
            self.barcode = f"AUTO_{str(uuid.uuid4())[:8]}"

        if not self.image:
            self.image.save("-".join(self.name_full.split() + [self.ORCID]).lower() + ".jpg", None)

        if not self.acronym:
            "" ' acronym shall consist of first letter of first name and two letters of last name, in upper case ""'
            i = 0
            j = 2

            if len(self.name_last) > 3:  # this is not the case for institues/companies
                self.acronym = "".join((self.name_first[i] + self.name_last[:j])).upper()

                # checking for uniqueness of entry
                while Entity.objects.filter(acronym=self.acronym).exists():
                    if i > len(self.name_last) - 1:
                        # ~ self.stderr.write(self.style.ERROR('ERROR: Auto acronym generation - no uniquie acronym can be generated !'))
                        logging.error(
                            "Auto acronym generation - no uniquie acronym can be generated for Entity [{} -{}], adding number ... ! ".format(
                                self.name_first, self.name_last
                            )
                        )
                        self.acronym = "".join(
                            (self.name_first[0] + self.name_last[i] + self.name_last[j]) + str(j)
                        ).upper()
                        j += 1
                    else:
                        self.acronym = "".join((self.name_first[0] + self.name_last[i] + self.name_last[j])).upper()

                    if j < len(self.name_last) - 1:
                        j += 1
                    else:
                        i += 1
                        j = i + 1

            elif len(self.name_full) > 3:  # institute or company
                name_list = self.name_full.split()
                while Entity.objects.filter(acronym=self.acronym).exists():
                    j += 1
                    try:
                        if len(name_list) > 2:
                            self.acronym = "".join([initials[i] for initials in name_list]).upper()
                        else:
                            # should be better selected, check, if unique....
                            self.acronym = self.name_full[:j].upper()
                    except Exception as err:
                        logging.error("Could not generate acronym {}".format(err))

        if self.IRI is None or self.IRI == "":
            unique_entity_name = "_".join((name_first, name_last, entity_id)).lower().strip()
            self.IRI = f"{settings.LARA_PREFIXES['lara']}people/Entity/{unique_entity_name}"
        if self.PID is None or self.PID == "":
            unique_entity_name = "_".join((name_first, name_last, entity_id)).lower().strip()
            self.PID = (
                # TODO: adjust to new PID system
                f"{settings.LARA_PID_PREFIXES['entity']}/{unique_entity_name}"
            )

        super().save(*args, **kwargs)

    def generate_barcode1D(
        self,
        barcode_format="code128",
        barcode_height=50,
        barcode_width=1,
        barcode_quiet_zone=10,
        barcode_human_readable=True,
        barcode_background_color="white",
        barcode_foreground_color="black",
        barcode_font_size=10,
        barcode_font_path=None,
        barcode_text=False,
        barcode_text_distance=5,
        barcode_text_position="bottom",
        output_format="png",
    ):
        """generates a 1D barcode for the entity,
        e.g. for printing on a badge or for scanning
        """
        pass

    def generate_barcode2D(
        self,
        barcode_format="qrcode",
        barcode_error_correction="L",
        barcode_box_size=10,
        barcode_border=4,
        barcode_version=None,
        barcode_mask_pattern=None,
        barcode_image_factory=None,
        barcode_background_color="white",
        barcode_foreground_color="black",
        barcode_text=False,
        barcode_text_distance=5,
        output_format="png",
    ):
        """generates a 2D barcode for the entity,
        e.g. for printing on a badge or for scanning
        """
        pass

    def __str__(self):
        return self.name_full or ""

    def __repr____(self):
        return f"{self.name_full} - [{self.name_first}|{self.name_last}] - ({self.acronym})" or ""


class Group(models.Model):
    """A group is a collection of entities, e.g. a working group, a project group, a group of developers, ..."""

    model_CURI = "lara:people/Group"
    group_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    namespace = models.ForeignKey(
        Namespace,
        related_name="%(app_label)s_%(class)s_namespaces_related",
        related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="namespace of group",
    )
    group_class = models.ForeignKey(
        EntityClass,
        related_name="%(app_label)s_%(class)s_group_class_related",
        related_query_name="%(app_label)s_%(class)s_group_class_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="class/type/position of the group, e.g., administrators, developers, ...",
    )
    name = models.TextField(help_text="name of group")

    name_full = models.TextField(
        unique=True,
        help_text="full name, is auto-generated when saved",
    )

    members = models.ManyToManyField(
        Entity,
        related_name="%(app_label)s_%(class)s_members_related",
        related_query_name="%(app_label)s_%(class)s_members_related_query",
        blank=True,
        help_text="members of group",
    )
    description = models.TextField(blank=True, help_text="description of group")
    remarks = models.TextField(blank=True, null=True, help_text="remarks about the group")

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for full_name
        """
        if self.name_full is None or self.name_full == "":
            if self.namespace is None:
                self.name_full = self.name.replace(" ", "_").strip().lower()
            else:
                self.name_full = f"{self.namespace.URI}/{self.name.replace(' ', '_').strip().lower()}"

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name or ""

    def __repr__(self) -> str:
        return self.name or ""


class EntityBankAccount(models.Model):
    """Bank account of an Entity."""

    model_CURI = "lara:people/EntityBankAccount"
    account_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    account_name = models.TextField(
        unique=True, null=True, help_text="label/name of bank account - not name of account holder !"
    )
    account_no = models.TextField(
        unique=True, null=True, help_text="Account Number - for flexibility this is a text field"
    )
    IBAN = models.TextField(unique=True, null=True, help_text="IBAN of account")
    BIC = models.TextField(unique=True, null=True, help_text="BIC of account")
    bank = models.ForeignKey(Entity, on_delete=models.CASCADE, blank=True, help_text="bank entity")
    description = models.TextField(blank=True, null=True, help_text="description/remarks of account")

    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_eba_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_eba_data_extra_related_query",
        blank=True,
        help_text="additions to bank account ...",
    )

    def __str__(self):
        return self.account_name or ""

    def __repr__(self):
        return self.account_name or ""


class LaraUser(models.Model):
    """lara user: an extension of the default django user model to cover entity information
    it extends the django.contrib.auth.models User model by an Entity."""

    model_CURI = "lara:people/LaraUser"
    lara_user_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    entity = models.ForeignKey(
        Entity,
        related_name="%(app_label)s_%(class)s_entities_related",
        related_query_name="%(app_label)s_%(class)s_entities_related_query",
        on_delete=models.CASCADE,
        blank=True,
        help_text="related Entity - storing the complete Affiliation info etc.",
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, help_text="back-reference to the Django user")
    home_screen_URL = models.URLField(
        blank=True,
        null=True,
        help_text="URL to the home screen - this is the default screen after login and can be changed by the user",
    )
    home_screen_layout = models.JSONField(blank=True, null=True, help_text="layout = what to see on the home screen")
    access_TOKEN = models.TextField(blank=True, help_text="LARA-django (API) access token")
    access_control = models.JSONField(blank=True, null=True, help_text="Access control, similar to Linux ACL")
    # email registration
    confirmation_TOKEN = models.TextField(blank=True, help_text="LARA-django e-mail confirmation token")
    # this shall be developed, when login strategies are improved
    email_confirmed = models.EmailField(blank=True, help_text="LARA user confirmed e-mail")
    email_recover = models.EmailField(blank=True, help_text="LARA user account recovering e-mail")
    max_logins = models.SmallIntegerField(blank=True, default=9, help_text="max number of logins")
    failed_logins = models.SmallIntegerField(blank=True, default=0, help_text="number of failed login attempts")
    IP_curr_login = models.TextField(blank=True, help_text="IP address of current  login")
    IP_last_login = models.TextField(blank=True, help_text="IP address of last login")
    datetime_confirmed = models.DateTimeField(blank=True, null=True, help_text="datetime of confirmation")
    datetime_confirmation_sent = models.DateTimeField(
        blank=True, null=True, help_text="datetime of confirmation was sent to the user"
    )

    def save(self, *args, **kwargs):
        if self.entity.email == "":
            self.entity.email = self.user.email

        if self.user.first_name == "":
            self.user.first_name = self.entity.name_first

        if self.user.last_name == "":
            self.user.last_name = self.entity.name_last

        if self.user.email == "":
            self.user.email = self.entity.email

        self.user.save()

        super().save(*args, **kwargs)

    def __str__(self):
        return self.user.username or ""

    class Meta:
        db_table = "lara_people_lara_user"


# Django Groups shall be used for grouping - please test, if this is enough and no extra LARA groups might be required ... ???


class MeetingsCalendar(CalendarAbstract):
    """Meeting Calendar"""

    model_CURI = "lara:people/MeetingsCalendar"
    calendar_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    organiser = models.ForeignKey(
        LaraUser,
        related_name="%(app_label)s_%(class)s_organiser_related",
        related_query_name="%(app_label)s_%(class)s_organiser_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="LARA User, who created the calendar entry",
    )
    attendees = models.ManyToManyField(
        LaraUser,
        related_name="%(app_label)s_%(class)s_attendees_meetings_calendar_related",
        related_query_name="%(app_label)s_%(class)s_attendees_meetings_calendar_related_query",
        blank=True,
        help_text="attendees of the meeting",
    )
