"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_people admin *

:details: lara_django_people admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator lara_django_people >> admin.py" to update this file
________________________________________________________________________
"""
# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import (
    ExtraData,
    EntityClass,
    EntityRole,
    EntityTitle,
    Entity,
    EntityBankAccount,
    Group,
    LaraUser,
    MeetingsCalendar,
)


@admin.register(ExtraData)
class ExtraDataAdmin(admin.ModelAdmin):
    list_display = (
        "data_type",
        "namespace",
        "media_type",
        "IRI",
        "URL",
        "description",
        "extradata_id",
        "file",
        "image",
        "office_room",
        "laboratory",
        "email",
    )
    list_filter = (
        "data_type",
        "namespace",
        "media_type",
        "office_room",
        "laboratory",
    )


@admin.register(EntityClass)
class EntityClassAdmin(admin.ModelAdmin):
    list_display = ("name", "description", "entity_class_id")
    search_fields = ("name",)


@admin.register(EntityTitle)
class EntityTitleAdmin(admin.ModelAdmin):
    list_display = ("title", "description", "entity_title_id")


@admin.register(EntityRole)
class EntityRoleAdmin(admin.ModelAdmin):
    list_display = ("role_id", "name", "description")


@admin.register(Entity)
class EntityAdmin(admin.ModelAdmin):
    list_display = (
        "name_full",
        "slug",
        "entity_class",
        "gender",
        "title",
        "name_first",
        "names_middle",
        "name_last",
        "names_last_previous",
        "name_nick",
        "acronym",
        "role_current",
        "URL",
        "ORCID",
        "ROR",
        "office_room",
        "laboratory",
        "email",
        "website",
        "phone_number_mobile",
        "phone_number_office",
        "phone_number_lab",
        "phone_number_home",
        "image",
        "entity_id",
    )
    list_filter = (
        "entity_class",
        "title",
        "role_current",
        "affiliation_current",
        "affiliation_start",
        "affiliation_end",
        "office_room",
        "laboratory",
        "address",
        "date_birth",
        "date_death",
    )
    raw_id_fields = (
        "bank_accounts",
        "expertise",
        "interests",
        "work_topics",
        "affiliation_previous",
        "data_extra",
    )
    search_fields = ("slug",)


@admin.register(EntityBankAccount)
class EntityBankAccountAdmin(admin.ModelAdmin):
    list_display = (
        "account_id",
        "account_name",
        "account_no",
        "IBAN",
        "BIC",
        "bank",
        "description",
    )
    list_filter = ("bank",)
    raw_id_fields = ("data_extra",)


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(LaraUser)
class LaraUserAdmin(admin.ModelAdmin):
    list_display = (
        "user",
        "entity",
        "access_TOKEN",
        "access_control",
        "confirmation_TOKEN",
        "email_confirmed",
        "email_recover",
        "max_logins",
        "failed_logins",
        "IP_curr_login",
        "IP_last_login",
        "datetime_confirmed",
        "datetime_confirmation_sent",
        "lara_user_id",
    )
    list_filter = (
        "entity",
        "user",
        "datetime_confirmed",
        "datetime_confirmation_sent",
    )


@admin.register(MeetingsCalendar)
class MeetingsCalendarAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "calendar_URL",
        "summary",
        "description",
        "color",
        "ics",
        "datetime_start",
        "datetime_end",
        "duration",
        "all_day",
        "created",
        "datetime_last_modified",
        "timestamp",
        "location",
        "geolocation",
        "conference_URL",
        "range",
        "related",
        "role",
        "tzid",
        "offset_UTC",
        "alarm_repeat_JSON",
        "event_repeat",
        "event_repeat_JSON",
        "organiser",
        "calendar_id",
    )
    list_filter = (
        "datetime_start",
        "datetime_end",
        "all_day",
        "created",
        "datetime_last_modified",
        "timestamp",
        "location",
        "geolocation",
        "organiser",
    )
    raw_id_fields = ("tags", "attendees")
