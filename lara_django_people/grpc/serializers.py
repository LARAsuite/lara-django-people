"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_myproj gRPC serializer*

:details: lara_django_myproj gRPC serializer.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

## generated with django-socio-grpc generateprpcinterface lara_django_people  (LARA-version)

import logging

from rest_framework.serializers import UUIDField, PrimaryKeyRelatedField
from django_socio_grpc import proto_serializers
import lara_django_people_grpc.v1.lara_django_people_pb2 as lara_django_people_pb2

from django.contrib.auth.models import User

from lara_django_base.models import (
    Namespace,
    DataType,
    MediaType,
    Namespace,
    Tag,
    ExternalResource,
    Room,
    Address,
    Location,
)
from lara_django_people.models import (
    ExtraData,
    EntityClass,
    EntityRole,
    EntityTitle,
    Entity,
    EntityBankAccount,
    Group,
    LaraUser,
    MeetingsCalendar,
)


class ExtradataProtoSerializer(proto_serializers.ModelProtoSerializer):
    data_type = PrimaryKeyRelatedField(queryset=DataType.objects.all(), pk_field=UUIDField(format="hex_verbose"))
    namespace = PrimaryKeyRelatedField(queryset=Namespace.objects.all(), pk_field=UUIDField(format="hex_verbose"))
    media_type = PrimaryKeyRelatedField(queryset=MediaType.objects.all(), pk_field=UUIDField(format="hex_verbose"))

    class Meta:
        model = ExtraData
        proto_class = lara_django_people_pb2.ExtradataResponse

        proto_class_list = lara_django_people_pb2.ExtradataListResponse

        fields = "__all__"  # [data_type', namespace', URI', text', XML', JSON', bin', media_type', IRI', URL', description', file', image', office_room', laboratory', email']


class EntityclassProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = EntityClass
        proto_class = lara_django_people_pb2.EntityclassResponse

        proto_class_list = lara_django_people_pb2.EntityclassListResponse

        fields = "__all__"  # [name', description']


class EntityroleProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = EntityRole
        # proto_class = lara_django_people_pb2.EntityroleResponse

        # proto_class_list = lara_django_people_pb2.EntityroleListResponse

        fields = "__all__"  # [name', description']


class EntitytitleProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = EntityTitle
        proto_class = lara_django_people_pb2.EntitytitleResponse

        proto_class_list = lara_django_people_pb2.EntitytitleListResponse

        fields = "__all__"  # [title', description']


class EntityProtoSerializer(proto_serializers.ModelProtoSerializer):
    entity_class = PrimaryKeyRelatedField(queryset=EntityClass.objects.all(), allow_null=True, required=False)
    title = PrimaryKeyRelatedField(queryset=EntityTitle.objects.all(), allow_null=True, required=False)
    bank_accounts = PrimaryKeyRelatedField(
        queryset=EntityBankAccount.objects.all(), allow_null=True, required=False, many=True
    )
    expertise = PrimaryKeyRelatedField(queryset=Tag.objects.all(), allow_null=True, required=False, many=True)
    interests = PrimaryKeyRelatedField(queryset=Tag.objects.all(), allow_null=True, required=False, many=True)
    work_topics = PrimaryKeyRelatedField(queryset=Tag.objects.all(), allow_null=True, required=False, many=True)
    roles = PrimaryKeyRelatedField(queryset=EntityRole.objects.all(), allow_null=True, required=False, many=True)
    role_current = PrimaryKeyRelatedField(queryset=EntityRole.objects.all(), allow_null=True, required=False)
    affiliation_current = PrimaryKeyRelatedField(queryset=Entity.objects.all(), allow_null=True, required=False)
    affiliation_previous = PrimaryKeyRelatedField(
        queryset=Entity.objects.all(), allow_null=True, required=False, many=True
    )
    office_room = PrimaryKeyRelatedField(queryset=Room.objects.all(), allow_null=True, required=False)
    laboratory = PrimaryKeyRelatedField(queryset=Room.objects.all(), allow_null=True, required=False)
    address = PrimaryKeyRelatedField(queryset=Address.objects.all(), allow_null=True, required=False)

    data_extra = PrimaryKeyRelatedField(queryset=ExtraData.objects.all(), allow_null=True, required=False, many=True)

    class Meta:
        model = Entity
        proto_class = lara_django_people_pb2.EntityResponse

        proto_class_list = lara_django_people_pb2.EntityListResponse

        fields = "__all__"  # [slug', entity_class', gender', title', name_first', names_middle', name_last', name_full', names_last_previous', name_nick', acronym', URL', handle', IRI', ORCID', ROR', TAX_no', VAT_no', TOLL_no', affiliation_current', affiliation_start', affiliation_end', office_room', laboratory', email', email_private', email_permanent', website', phone_number_mobile', phone_number_office', phone_number_lab', phone_number_home', address', date_birth', date_death', color', icon', image']


class GroupProtoSerializer(proto_serializers.ModelProtoSerializer):
    namespace = PrimaryKeyRelatedField(queryset=Namespace.objects.all(), allow_null=True, required=False)
    group_class = PrimaryKeyRelatedField(queryset=EntityClass.objects.all(), allow_null=True, required=False)
    members = PrimaryKeyRelatedField(queryset=Entity.objects.all(), allow_null=True, required=False, many=True)

    class Meta:
        model = Group
        # proto_class = lara_django_people_pb2.GroupResponse

        # proto_class_list = lara_django_people_pb2.GroupListResponse

        fields = "__all__"  # [name', description']


class EntitybankaccountProtoSerializer(proto_serializers.ModelProtoSerializer):
    bank = PrimaryKeyRelatedField(queryset=Entity.objects.all(), allow_null=True, required=False)
    data_extra = PrimaryKeyRelatedField(queryset=ExtraData.objects.all(), allow_null=True, required=False, many=True)

    class Meta:
        model = EntityBankAccount
        proto_class = lara_django_people_pb2.EntitybankaccountResponse

        proto_class_list = lara_django_people_pb2.EntitybankaccountListResponse

        fields = "__all__"  # [account_name', account_no', IBAN', BIC', bank', description']


class LarauserProtoSerializer(proto_serializers.ModelProtoSerializer):
    entity = PrimaryKeyRelatedField(queryset=Entity.objects.all(), allow_null=True, required=False)
    user = PrimaryKeyRelatedField(queryset=User.objects.all(), allow_null=True, required=False)

    class Meta:
        model = LaraUser
        proto_class = lara_django_people_pb2.LarauserResponse

        proto_class_list = lara_django_people_pb2.LarauserListResponse

        fields = "__all__"  # [entity', user', welcome_screen_layout', access_TOKEN', access_control', confirmation_TOKEN', email_confirmed', email_recover', max_logins', failed_logins', IP_curr_login', IP_last_login', datetime_confirmed', datetime_confirmation_sent']


class MeetingscalendarProtoSerializer(proto_serializers.ModelProtoSerializer):
    organiser = PrimaryKeyRelatedField(queryset=LaraUser.objects.all(), allow_null=True, required=False)
    attendees = PrimaryKeyRelatedField(queryset=LaraUser.objects.all(), allow_null=True, required=False, many=True)

    class Meta:
        model = MeetingsCalendar
        proto_class = lara_django_people_pb2.MeetingscalendarResponse

        proto_class_list = lara_django_people_pb2.MeetingscalendarListResponse

        fields = "__all__"  # [title', calendar_URL', summary', description', color', ics', datetime_start', datetime_end', duration', all_day', created', last_modified', timestamp', location', geolocation', conference_URL', range', related', role', tzid', offset_UTC', alarm_repeat_JSON', event_repeat', event_repeat_JSON', organiser']
