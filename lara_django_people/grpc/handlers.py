"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_myproj gRPC handlers*

:details: lara_django_myproj gRPC handlers.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

# generated with django-socio-grpc generateprpcinterface lara_django_people  (LARA-version)

#import logging
from django_socio_grpc.services.app_handler_registry import AppHandlerRegistry
from lara_django_people.grpc.services import ExtradataService, EntityclassService, EntitytitleService, EntityService, EntitybankaccountService, GroupService, LarauserService, MeetingscalendarService

def grpc_handlers(server):
    app_registry = AppHandlerRegistry("lara_django_people", server)

    app_registry.get_grpc_module = lambda: "lara_django_people_grpc.v1"

    app_registry.register(ExtradataService)

    app_registry.register(EntityclassService)

    app_registry.register(EntitytitleService)

    app_registry.register(EntityService)

    app_registry.register(EntitybankaccountService)

    #app_registry.register(GroupService)

    app_registry.register(LarauserService)

    app_registry.register(MeetingscalendarService)
