"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_myproj gRPC services*

:details: lara_django_myproj gRPC services.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

## generated with django-socio-grpc generateprpcinterface lara_django_people  (LARA-version)

from django_socio_grpc import generics, mixins
from .serializers import (
    ExtradataProtoSerializer,
    EntityclassProtoSerializer,
    EntityroleProtoSerializer,
    EntitytitleProtoSerializer,
    EntityProtoSerializer,
    EntitybankaccountProtoSerializer,
    GroupProtoSerializer,
    LarauserProtoSerializer,
    MeetingscalendarProtoSerializer,
)

from ..filters import EntityFilterSet, GroupFilterSet, MeetingsCalendarFilterSet

from lara_django_people.models import (
    ExtraData,
    EntityClass,
    EntityRole,
    EntityTitle,
    Entity,
    EntityBankAccount,
    Group,
    LaraUser,
    MeetingsCalendar,
)


class ExtradataService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = ExtraData.objects.all()
    serializer_class = ExtradataProtoSerializer


class EntityclassService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = EntityClass.objects.all()
    serializer_class = EntityclassProtoSerializer

class EntityroleService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = EntityRole.objects.all()
    serializer_class = EntityroleProtoSerializer

class EntitytitleService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = EntityTitle.objects.all()
    serializer_class = EntitytitleProtoSerializer


class EntityService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = Entity.objects.all()
    serializer_class = EntityProtoSerializer
    filterset_class = EntityFilterSet


class GroupService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = Group.objects.all()
    serializer_class = GroupProtoSerializer
    filterset_class = GroupFilterSet


class EntitybankaccountService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = EntityBankAccount.objects.all()
    serializer_class = EntitybankaccountProtoSerializer


class LarauserService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = LaraUser.objects.all()
    serializer_class = LarauserProtoSerializer


class MeetingscalendarService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = MeetingsCalendar.objects.all()
    serializer_class = MeetingscalendarProtoSerializer
    filterset_class = MeetingsCalendarFilterSet
