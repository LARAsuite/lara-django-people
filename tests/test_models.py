"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_people tests *

:details: lara_django_people application models tests.
         - 
:authors: mark doerr  <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase

# from lara_django_people.models import

# Create your lara_django_people tests here.
from ..models import City, CountrySubdivision, Country, GeoLocation, Entity, Address, LaraUser

logging.basicConfig(
    format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
#~ logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.ERROR)


