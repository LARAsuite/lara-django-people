
# History  of lara_django_people 


* 0.2.20 (2023-04-30)


* First release ....

v0.2.44
-------

Bug fixes:

 - 14e7fb2 Models, api

Other changes:

 - 0174e68 Bump version: 0.2.43 → 0.2.44

v0.2.47
-------

Features:

 - 5713d7d Group model improved; Entity role and role_current added; serializers added

Other changes:

 - aed62b1 Bump version: 0.2.46 → 0.2.47

v0.2.48
-------

Bug fixes:

 - e4f2d64 Filters.py; forms.py;
 - 34ebe97 Forms.py
 - 6c5f662 Forms.py updated
 - 7e0e911 Forms.py updated
 - 9eb511d Forms.py updated
 - a5d2eec GRPC API updated
 - 4d96edb HISTORY.md
 - cd4ae4d Literature->resources_external; handle->PID; create & update view;

Other changes:

 - 5b0d648 Bump version: 0.2.47 → 0.2.48

v0.2.49
-------

Features:

 - e2e66cd Admin.py entity role added; fixtures added; filter.py fixed

Bug fixes:

 - 7d7983a Fixture entity_class
 - 15874b4 GRPC API updated
 - 16b403d GRPC API updated
 - e9bf048 HISTORY.md

Other changes:

 - 7454b16 Bump version: 0.2.48 → 0.2.49
 - e8e40cc Improved model

v0.2.50
-------

Bug fixes:

 - 13e7510 GRPC API updated
 - af00995 HISTORY.md

Other changes:

 - e42f1bb Bump version: 0.2.49 → 0.2.50

v0.2.51
-------

Bug fixes:

 - c5faa8e GRPC API updated
 - e70a79d HISTORY.md

Other changes:

 - 15f4ad6 Bump version: 0.2.50 → 0.2.51

v0.2.52
-------

Bug fixes:

 - 2aa4427 GRPC API updated
 - 5ccee6c HISTORY.md

Other changes:

 - cc8fab9 Bump version: 0.2.51 → 0.2.52

v0.2.53
-------

Bug fixes:

 - db15a7c GRPC API updated
 - 52f40cf HISTORY.md

Other changes:

 - ab798df Bump version: 0.2.52 → 0.2.53

v0.2.54
-------

Bug fixes:

 - f762a62 HISTORY.md
 - 6f4bf0c Static files moved to subfolder; default home_screen

Other changes:

 - f2f8b5f Bump version: 0.2.53 → 0.2.54

v0.2.55
-------

Bug fixes:

 - a72e0e1 GRPC API updated
 - a1c754b HISTORY.md

Other changes:

 - 1aa6a9e Bump version: 0.2.54 → 0.2.55

v0.2.56
-------

Bug fixes:

 - 218f8de GRPC API updated
 - 918d947 GRPC API updated
 - acea520 GRPC API updated
 - c8f89d6 HISTORY.md
 - 1338775 More consistent naming of variables; improved gRPC interface.

Other changes:

 - c99b9f6 Bump version: 0.2.55 → 0.2.56

v0.2.57
-------

Features:

 - 62da66c New LinkML schema added
 - a66ec8f New LinkML schema added
 - fb451f8 New LinkML schema added
 - 4896ffe New LinkML schema, data model and high-level interface added

Bug fixes:

 - 96d1036 GRPC API updated
 - a00fa5a HISTORY.md
 - ae55990 More consistent naming of variables; improved gRPC interface.
 - f4aaebe Views - Groups added

Other changes:

 - 1c2ed87 Bump version: 0.2.56 → 0.2.57

v0.2.58
-------

Bug fixes:

 - f5b92dd GRPC API updated
 - 85d1def HISTORY.md
 - b2750fb Improved model and schema; remarks and description added
 - c4578fe More consistent naming of variables; improved gRPC interface.

Other changes:

 - 883361b Bump version: 0.2.57 → 0.2.58

