// @generated by protoc-gen-connect-es v0.11.0
// @generated from file lara_django_people_grpc/v1/lara_django_people.proto (package lara_django.lara_django_people, syntax proto3)
/* eslint-disable */
// @ts-nocheck

import { EntitybankaccountDestroyRequest, EntitybankaccountListRequest, EntitybankaccountListResponse, EntitybankaccountPartialUpdateRequest, EntitybankaccountRequest, EntitybankaccountResponse, EntitybankaccountRetrieveRequest, EntitybankaccountStreamRequest, EntityclassDestroyRequest, EntityclassListRequest, EntityclassListResponse, EntityclassPartialUpdateRequest, EntityclassRequest, EntityclassResponse, EntityclassRetrieveRequest, EntityclassStreamRequest, EntityDestroyRequest, EntityListRequest, EntityListResponse, EntityPartialUpdateRequest, EntityRequest, EntityResponse, EntityRetrieveRequest, EntityStreamRequest, EntitytitleDestroyRequest, EntitytitleListRequest, EntitytitleListResponse, EntitytitlePartialUpdateRequest, EntitytitleRequest, EntitytitleResponse, EntitytitleRetrieveRequest, EntitytitleStreamRequest, ExtradataDestroyRequest, ExtradataListRequest, ExtradataListResponse, ExtradataPartialUpdateRequest, ExtradataRequest, ExtradataResponse, ExtradataRetrieveRequest, ExtradataStreamRequest, LarauserDestroyRequest, LarauserListRequest, LarauserListResponse, LarauserPartialUpdateRequest, LarauserRequest, LarauserResponse, LarauserRetrieveRequest, LarauserStreamRequest, MeetingscalendarDestroyRequest, MeetingscalendarListRequest, MeetingscalendarListResponse, MeetingscalendarPartialUpdateRequest, MeetingscalendarRequest, MeetingscalendarResponse, MeetingscalendarRetrieveRequest, MeetingscalendarStreamRequest } from "./lara_django_people_pb.js";
import { Empty, MethodKind } from "@bufbuild/protobuf";

/**
 * @generated from service lara_django.lara_django_people.EntityController
 */
export declare const EntityController: {
  readonly typeName: "lara_django.lara_django_people.EntityController",
  readonly methods: {
    /**
     * @generated from rpc lara_django.lara_django_people.EntityController.Create
     */
    readonly create: {
      readonly name: "Create",
      readonly I: typeof EntityRequest,
      readonly O: typeof EntityResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntityController.Destroy
     */
    readonly destroy: {
      readonly name: "Destroy",
      readonly I: typeof EntityDestroyRequest,
      readonly O: typeof Empty,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntityController.List
     */
    readonly list: {
      readonly name: "List",
      readonly I: typeof EntityListRequest,
      readonly O: typeof EntityListResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntityController.PartialUpdate
     */
    readonly partialUpdate: {
      readonly name: "PartialUpdate",
      readonly I: typeof EntityPartialUpdateRequest,
      readonly O: typeof EntityResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntityController.Retrieve
     */
    readonly retrieve: {
      readonly name: "Retrieve",
      readonly I: typeof EntityRetrieveRequest,
      readonly O: typeof EntityResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntityController.Stream
     */
    readonly stream: {
      readonly name: "Stream",
      readonly I: typeof EntityStreamRequest,
      readonly O: typeof EntityResponse,
      readonly kind: MethodKind.ServerStreaming,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntityController.Update
     */
    readonly update: {
      readonly name: "Update",
      readonly I: typeof EntityRequest,
      readonly O: typeof EntityResponse,
      readonly kind: MethodKind.Unary,
    },
  }
};

/**
 * @generated from service lara_django.lara_django_people.EntitybankaccountController
 */
export declare const EntitybankaccountController: {
  readonly typeName: "lara_django.lara_django_people.EntitybankaccountController",
  readonly methods: {
    /**
     * @generated from rpc lara_django.lara_django_people.EntitybankaccountController.Create
     */
    readonly create: {
      readonly name: "Create",
      readonly I: typeof EntitybankaccountRequest,
      readonly O: typeof EntitybankaccountResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntitybankaccountController.Destroy
     */
    readonly destroy: {
      readonly name: "Destroy",
      readonly I: typeof EntitybankaccountDestroyRequest,
      readonly O: typeof Empty,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntitybankaccountController.List
     */
    readonly list: {
      readonly name: "List",
      readonly I: typeof EntitybankaccountListRequest,
      readonly O: typeof EntitybankaccountListResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntitybankaccountController.PartialUpdate
     */
    readonly partialUpdate: {
      readonly name: "PartialUpdate",
      readonly I: typeof EntitybankaccountPartialUpdateRequest,
      readonly O: typeof EntitybankaccountResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntitybankaccountController.Retrieve
     */
    readonly retrieve: {
      readonly name: "Retrieve",
      readonly I: typeof EntitybankaccountRetrieveRequest,
      readonly O: typeof EntitybankaccountResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntitybankaccountController.Stream
     */
    readonly stream: {
      readonly name: "Stream",
      readonly I: typeof EntitybankaccountStreamRequest,
      readonly O: typeof EntitybankaccountResponse,
      readonly kind: MethodKind.ServerStreaming,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntitybankaccountController.Update
     */
    readonly update: {
      readonly name: "Update",
      readonly I: typeof EntitybankaccountRequest,
      readonly O: typeof EntitybankaccountResponse,
      readonly kind: MethodKind.Unary,
    },
  }
};

/**
 * @generated from service lara_django.lara_django_people.EntityclassController
 */
export declare const EntityclassController: {
  readonly typeName: "lara_django.lara_django_people.EntityclassController",
  readonly methods: {
    /**
     * @generated from rpc lara_django.lara_django_people.EntityclassController.Create
     */
    readonly create: {
      readonly name: "Create",
      readonly I: typeof EntityclassRequest,
      readonly O: typeof EntityclassResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntityclassController.Destroy
     */
    readonly destroy: {
      readonly name: "Destroy",
      readonly I: typeof EntityclassDestroyRequest,
      readonly O: typeof Empty,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntityclassController.List
     */
    readonly list: {
      readonly name: "List",
      readonly I: typeof EntityclassListRequest,
      readonly O: typeof EntityclassListResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntityclassController.PartialUpdate
     */
    readonly partialUpdate: {
      readonly name: "PartialUpdate",
      readonly I: typeof EntityclassPartialUpdateRequest,
      readonly O: typeof EntityclassResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntityclassController.Retrieve
     */
    readonly retrieve: {
      readonly name: "Retrieve",
      readonly I: typeof EntityclassRetrieveRequest,
      readonly O: typeof EntityclassResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntityclassController.Stream
     */
    readonly stream: {
      readonly name: "Stream",
      readonly I: typeof EntityclassStreamRequest,
      readonly O: typeof EntityclassResponse,
      readonly kind: MethodKind.ServerStreaming,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntityclassController.Update
     */
    readonly update: {
      readonly name: "Update",
      readonly I: typeof EntityclassRequest,
      readonly O: typeof EntityclassResponse,
      readonly kind: MethodKind.Unary,
    },
  }
};

/**
 * @generated from service lara_django.lara_django_people.EntitytitleController
 */
export declare const EntitytitleController: {
  readonly typeName: "lara_django.lara_django_people.EntitytitleController",
  readonly methods: {
    /**
     * @generated from rpc lara_django.lara_django_people.EntitytitleController.Create
     */
    readonly create: {
      readonly name: "Create",
      readonly I: typeof EntitytitleRequest,
      readonly O: typeof EntitytitleResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntitytitleController.Destroy
     */
    readonly destroy: {
      readonly name: "Destroy",
      readonly I: typeof EntitytitleDestroyRequest,
      readonly O: typeof Empty,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntitytitleController.List
     */
    readonly list: {
      readonly name: "List",
      readonly I: typeof EntitytitleListRequest,
      readonly O: typeof EntitytitleListResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntitytitleController.PartialUpdate
     */
    readonly partialUpdate: {
      readonly name: "PartialUpdate",
      readonly I: typeof EntitytitlePartialUpdateRequest,
      readonly O: typeof EntitytitleResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntitytitleController.Retrieve
     */
    readonly retrieve: {
      readonly name: "Retrieve",
      readonly I: typeof EntitytitleRetrieveRequest,
      readonly O: typeof EntitytitleResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntitytitleController.Stream
     */
    readonly stream: {
      readonly name: "Stream",
      readonly I: typeof EntitytitleStreamRequest,
      readonly O: typeof EntitytitleResponse,
      readonly kind: MethodKind.ServerStreaming,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.EntitytitleController.Update
     */
    readonly update: {
      readonly name: "Update",
      readonly I: typeof EntitytitleRequest,
      readonly O: typeof EntitytitleResponse,
      readonly kind: MethodKind.Unary,
    },
  }
};

/**
 * @generated from service lara_django.lara_django_people.ExtradataController
 */
export declare const ExtradataController: {
  readonly typeName: "lara_django.lara_django_people.ExtradataController",
  readonly methods: {
    /**
     * @generated from rpc lara_django.lara_django_people.ExtradataController.Create
     */
    readonly create: {
      readonly name: "Create",
      readonly I: typeof ExtradataRequest,
      readonly O: typeof ExtradataResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.ExtradataController.Destroy
     */
    readonly destroy: {
      readonly name: "Destroy",
      readonly I: typeof ExtradataDestroyRequest,
      readonly O: typeof Empty,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.ExtradataController.List
     */
    readonly list: {
      readonly name: "List",
      readonly I: typeof ExtradataListRequest,
      readonly O: typeof ExtradataListResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.ExtradataController.PartialUpdate
     */
    readonly partialUpdate: {
      readonly name: "PartialUpdate",
      readonly I: typeof ExtradataPartialUpdateRequest,
      readonly O: typeof ExtradataResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.ExtradataController.Retrieve
     */
    readonly retrieve: {
      readonly name: "Retrieve",
      readonly I: typeof ExtradataRetrieveRequest,
      readonly O: typeof ExtradataResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.ExtradataController.Stream
     */
    readonly stream: {
      readonly name: "Stream",
      readonly I: typeof ExtradataStreamRequest,
      readonly O: typeof ExtradataResponse,
      readonly kind: MethodKind.ServerStreaming,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.ExtradataController.Update
     */
    readonly update: {
      readonly name: "Update",
      readonly I: typeof ExtradataRequest,
      readonly O: typeof ExtradataResponse,
      readonly kind: MethodKind.Unary,
    },
  }
};

/**
 * @generated from service lara_django.lara_django_people.LarauserController
 */
export declare const LarauserController: {
  readonly typeName: "lara_django.lara_django_people.LarauserController",
  readonly methods: {
    /**
     * @generated from rpc lara_django.lara_django_people.LarauserController.Create
     */
    readonly create: {
      readonly name: "Create",
      readonly I: typeof LarauserRequest,
      readonly O: typeof LarauserResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.LarauserController.Destroy
     */
    readonly destroy: {
      readonly name: "Destroy",
      readonly I: typeof LarauserDestroyRequest,
      readonly O: typeof Empty,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.LarauserController.List
     */
    readonly list: {
      readonly name: "List",
      readonly I: typeof LarauserListRequest,
      readonly O: typeof LarauserListResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.LarauserController.PartialUpdate
     */
    readonly partialUpdate: {
      readonly name: "PartialUpdate",
      readonly I: typeof LarauserPartialUpdateRequest,
      readonly O: typeof LarauserResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.LarauserController.Retrieve
     */
    readonly retrieve: {
      readonly name: "Retrieve",
      readonly I: typeof LarauserRetrieveRequest,
      readonly O: typeof LarauserResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.LarauserController.Stream
     */
    readonly stream: {
      readonly name: "Stream",
      readonly I: typeof LarauserStreamRequest,
      readonly O: typeof LarauserResponse,
      readonly kind: MethodKind.ServerStreaming,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.LarauserController.Update
     */
    readonly update: {
      readonly name: "Update",
      readonly I: typeof LarauserRequest,
      readonly O: typeof LarauserResponse,
      readonly kind: MethodKind.Unary,
    },
  }
};

/**
 * @generated from service lara_django.lara_django_people.MeetingscalendarController
 */
export declare const MeetingscalendarController: {
  readonly typeName: "lara_django.lara_django_people.MeetingscalendarController",
  readonly methods: {
    /**
     * @generated from rpc lara_django.lara_django_people.MeetingscalendarController.Create
     */
    readonly create: {
      readonly name: "Create",
      readonly I: typeof MeetingscalendarRequest,
      readonly O: typeof MeetingscalendarResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.MeetingscalendarController.Destroy
     */
    readonly destroy: {
      readonly name: "Destroy",
      readonly I: typeof MeetingscalendarDestroyRequest,
      readonly O: typeof Empty,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.MeetingscalendarController.List
     */
    readonly list: {
      readonly name: "List",
      readonly I: typeof MeetingscalendarListRequest,
      readonly O: typeof MeetingscalendarListResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.MeetingscalendarController.PartialUpdate
     */
    readonly partialUpdate: {
      readonly name: "PartialUpdate",
      readonly I: typeof MeetingscalendarPartialUpdateRequest,
      readonly O: typeof MeetingscalendarResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.MeetingscalendarController.Retrieve
     */
    readonly retrieve: {
      readonly name: "Retrieve",
      readonly I: typeof MeetingscalendarRetrieveRequest,
      readonly O: typeof MeetingscalendarResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.MeetingscalendarController.Stream
     */
    readonly stream: {
      readonly name: "Stream",
      readonly I: typeof MeetingscalendarStreamRequest,
      readonly O: typeof MeetingscalendarResponse,
      readonly kind: MethodKind.ServerStreaming,
    },
    /**
     * @generated from rpc lara_django.lara_django_people.MeetingscalendarController.Update
     */
    readonly update: {
      readonly name: "Update",
      readonly I: typeof MeetingscalendarRequest,
      readonly O: typeof MeetingscalendarResponse,
      readonly kind: MethodKind.Unary,
    },
  }
};

