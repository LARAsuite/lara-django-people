from __future__ import annotations
from datetime import datetime, date
from enum import Enum
from typing import List, Dict, Optional, Any, Union
from pydantic import BaseModel as BaseModel, ConfigDict,  Field, field_validator
import re
import sys
if sys.version_info >= (3, 8):
    from typing import Literal
else:
    from typing_extensions import Literal


metamodel_version = "None"
version = "None"

class ConfiguredBaseModel(BaseModel):
    model_config = ConfigDict(
        validate_assignment=True,
        validate_default=True,
        extra = 'forbid',
        arbitrary_types_allowed=True,
        use_enum_values = True)


class MediaType(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.MediaType'>
    """
    None
    
        

class Address(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.Address'>
    """
    None
    
        

class Tag(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.Tag'>
    """
    None
    
        

class Location(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.Location'>
    """
    None
    
        

class Namespace(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.Namespace'>
    """
    None
    
        

class GeoLocation(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.GeoLocation'>
    """
    None
    
        

class User(ConfiguredBaseModel):
    """
    <class 'django.contrib.auth.models.User'>
    """
    None
    
        

class Room(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.Room'>
    """
    None
    
        

class DataType(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.DataType'>
    """
    None
    
        

class ExtraData(ConfiguredBaseModel):
    """
    \" This class can be used to extend data, by extra information,
    e.g., more telephone numbers, customer numbers, ...
\"
    """
    data_type: Optional[DataType] = Field(None)
    namespace: Optional[Namespace] = Field(None, description="""namespace of data""")
    title: Optional[str] = Field(None, description="""Human readable title of extra data""")
    name: Optional[str] = Field(None, description="""name of extra data""")
    name_full: Optional[str] = Field(None, description="""full name of extra data""")
    version: Optional[str] = Field(None, description="""version of extra data""")
    UUID: str = Field(..., description="""data UUID""")
    hash_SHA256: Optional[str] = Field(None, description="""SHA256 hash of all data (JSON and XML)""")
    data_JSON: Optional[str] = Field(None, description="""generic JSON field""")
    media_type: Optional[MediaType] = Field(None, description="""IANA media type of extra data file""")
    IRI: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """)
    URL: Optional[str] = Field(None, description="""Universal Resource Locator - this can be used to link the data to external locations""")
    datetime_created: date = Field(..., description="""date and time when data was created""")
    datetime_last_modified: date = Field(..., description="""date and time when data was last modified""")
    description: Optional[str] = Field(None, description="""description of extra data""")
    extradata_id: Optional[str] = Field(None, description="""ExtraData - primary key (UUID)""")
    file: Optional[str] = Field(None, description="""rel. path/filename""")
    image: Optional[str] = Field(None, description="""location room map rel. path/filename to image""")
    office_room: Optional[Room] = Field(None, description="""extra office""")
    laboratory: Optional[Room] = Field(None, description="""extra laboratory""")
    email: Optional[str] = Field(None, description="""extra email""")
    
        

class EntityClass(ConfiguredBaseModel):
    """
    \" Entity class, like university, institute, company, organisation,
    but also guest_scientist, bachelor student, master, postdoc, group leader.
\"
    """
    entity_class_id: Optional[str] = Field(None, description="""EntityClass - primary key (UUID)""")
    name: str = Field(..., description="""entity class, like university, institute, company, organisation""")
    IRI: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """)
    description: Optional[str] = Field(None, description="""description of the entity class""")
    
        

class EntityRole(ConfiguredBaseModel):
    """
    \" Entity role, like scientist, reviewer, developer, maintainer, student, postdoc, project leader, ... \"
    """
    role_id: Optional[str] = Field(None, description="""EntityRole - primary key (UUID)""")
    name: str = Field(..., description="""entity role, like guest_scientist,  bachelor student, master, postdoc, ...""")
    IRI: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """)
    description: Optional[str] = Field(None, description="""description of the entity role""")
    
        

class EntityTitle(ConfiguredBaseModel):
    """
    \" Entity title, like academic title, e.g., Dr., Prof., Dr. ... \"
    """
    entity_title_id: Optional[str] = Field(None, description="""EntityTitle - primary key (UUID)""")
    title: Optional[str] = Field(None, description="""entity title, like academic title, e.g. Dr., Prof., Dr. ... """)
    description: Optional[str] = Field(None, description="""description of the entity title""")
    
        

class Entity(ConfiguredBaseModel):
    """
    \" Generic Entity Model. An Entity could be, e.g., a person or institution or company.
    The term \"entity\" encounters for this generalisation.
\"
    """
    entity_id: Optional[str] = Field(None, description="""Entity - primary key (UUID)""")
    slug: Optional[str] = Field(None, description="""slugs are used to display the entity in, e.g. URIs, auto generated, do not use any special characters""")
    entity_class: Optional[EntityClass] = Field(None, description="""class/type/position of the entity, e.g., university, institute, company""")
    gender: str = Field(...)
    title: Optional[EntityTitle] = Field(None, description="""entity title, e.g. Dr., Prof. Dr. """)
    name_first: Optional[str] = Field(None, description="""first name of a person or name of an institution/company""")
    names_middle: Optional[str] = Field(None, description="""list of middle names separated by space, e.g. Alfred Ernst Walter Neumann -> Ernst Walter""")
    name_last: Optional[str] = Field(None, description="""last name, e.g. vonSydow - aristocratic prepositions are part of the last name""")
    name_full: Optional[str] = Field(None, description="""full name, e.g. University of Greifswald, Institute for Biochemistry, Max vonSydow, is auto-generated when saved""")
    names_last_previous: Optional[str] = Field(None, description="""list of previous last names separated by space, e.g. changed after marriage, order: latest -> earliest""")
    name_nick: Optional[str] = Field(None, description="""the entities nickname, e.g., James -> Jim.""")
    acronym: Optional[str] = Field(None, description="""3 letter code, autogenerated, should be unique with an institution, could also be used for company acronyms""")
    URL: Optional[str] = Field(None, description="""Universal Resource Locator - URL, web address of entity""")
    PID: Optional[str] = Field(None, description="""handle URI""")
    IRI: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """)
    ORCID: Optional[str] = Field(None, description="""ORCID is a unique researcher ID (just the ID, not the URL), s. https://orcid.org/""")
    ROR: Optional[str] = Field(None, description="""ROR - Research Organization Registry s. https: // ror.readme.io/docs/ror-basics""")
    barcode: Optional[str] = Field(None, description="""text representation of a barcode of the sample.""")
    barcode_JSON: Optional[str] = Field(None, description="""Advanced barcodes in JSON for multiple barcode representations, like QR codes etc.""")
    TAX_no: Optional[str] = Field(None, description="""Tax number""")
    VAT_no: Optional[str] = Field(None, description="""Value Added Tax number""")
    TOLL_no: Optional[str] = Field(None, description="""Toll/EORI number""")
    role_current: Optional[EntityRole] = Field(None, description="""current role of the entity, e.g. project leader, technician postdoc, bachelor student, master student, guest scientist, ...""")
    affiliation_current: Optional[Entity] = Field(None, description="""affiliation of the entity""")
    affiliation_start: date = Field(..., description="""start date at this affiliation, default: date of today""")
    affiliation_end: date = Field(..., description="""end date at this affiliation, default: 9.9.9999""")
    office_room: Optional[Room] = Field(None, description="""office""")
    laboratory: Optional[Room] = Field(None, description="""laboratory""")
    email: Optional[str] = Field(None, description="""entity's current official email address""")
    email_private: Optional[str] = Field(None, description="""entity's email address""")
    email_permanent: Optional[str] = Field(None, description="""entity's permanents email address""")
    website: Optional[str] = Field(None, description="""entity's website URL""")
    phone_number_mobile: Optional[str] = Field(None, description="""entity's mobile phone number, if possible add the +Country code notation, e.g., +44 3834 420 4411""")
    phone_number_office: Optional[str] = Field(None, description="""entity's office phone number, if possible add the +Country code notation, e.g., +44 3834 420 4411""")
    phone_number_lab: Optional[str] = Field(None, description="""entity's lab phone number, if possible add the +Country code notation,e.g., +44 3834 420 4411""")
    phone_number_home: Optional[str] = Field(None, description="""entity's phone number at home, if possible add the +Country code notation,e.g., +44 3834 420 4411""")
    address: Optional[Address] = Field(None, description="""reference to an address""")
    date_birth: Optional[date] = Field(None, description="""date of birth of the person, could also be the founding date of an institution.""")
    date_death: Optional[date] = Field(None, description="""date of death, could also be the closing date of an institution.""")
    color: Optional[str] = Field(None, description="""GUI colour representation of the entity""")
    icon: Optional[str] = Field(None, description="""XML/SVG icon/logo/avatar/drawing of the entity""")
    image: Optional[str] = Field(None, description="""entity's image, rel. path/filename to image""")
    datetime_last_modified: date = Field(..., description="""date and time when data was last modified""")
    remarks: Optional[str] = Field(None, description="""remarks about the entity""")
    bank_accounts: Optional[List[EntityBankAccount]] = Field(default_factory=list, description="""expertise/skills/profession, keywords""")
    expertise: Optional[List[Tag]] = Field(default_factory=list, description="""expertise/skills/profession, keywords""")
    interests: Optional[List[Tag]] = Field(default_factory=list, description="""scientific interests, keywords""")
    work_topics: Optional[List[Tag]] = Field(default_factory=list, description="""work/research topics, keywords, e.g. transaminases, electrochemistry, fuel cell development""")
    roles: Optional[List[EntityRole]] = Field(default_factory=list, description="""roles of the entity, e.g. project leader, postdoc, bachelor student, master student, guest scientist, ...""")
    affiliation_previous: Optional[List[Entity]] = Field(default_factory=list, description="""previous affiliations of the entity""")
    data_extra: Optional[List[ExtraData]] = Field(default_factory=list, description="""e.g. ResearcherID, Scopus Author ID, extra laboratories ...""")
    
        

class Group(ConfiguredBaseModel):
    """
    \" A group is a collection of entities, e.g. a working group, a project group, a group of developers, ... \"
    """
    group_id: Optional[str] = Field(None, description="""Group - primary key (UUID)""")
    namespace: Optional[Namespace] = Field(None, description="""namespace of group""")
    group_class: Optional[EntityClass] = Field(None, description="""class/type/position of the group, e.g., administrators, developers, ...""")
    name: str = Field(..., description="""name of group""")
    name_full: str = Field(..., description="""full name, is auto-generated when saved""")
    description: Optional[str] = Field(None, description="""description of group""")
    remarks: Optional[str] = Field(None, description="""remarks about the group""")
    members: Optional[List[Entity]] = Field(default_factory=list, description="""members of group""")
    
        

class EntityBankAccount(ConfiguredBaseModel):
    """
    \" Bank account of an Entity. \"
    """
    account_id: Optional[str] = Field(None, description="""EntityBankAccount - primary key (UUID)""")
    account_name: str = Field(..., description="""label/name of bank account - not name of account holder !""")
    account_no: str = Field(..., description="""Account Number - for flexibility this is a text field""")
    IBAN: str = Field(..., description="""IBAN of account""")
    BIC: str = Field(..., description="""BIC of account""")
    bank: Optional[Entity] = Field(None, description="""bank entity""")
    description: Optional[str] = Field(None, description="""description/remarks of account""")
    data_extra: Optional[List[ExtraData]] = Field(default_factory=list, description="""additions to bank account ...""")
    
        

class LaraUser(ConfiguredBaseModel):
    """
    \" lara user: an extension of the default django user model to cover entity information
    it extends the django.contrib.auth.models User model by an Entity.
\"
    """
    lara_user_id: Optional[str] = Field(None, description="""LaraUser - primary key (UUID)""")
    entity: Optional[Entity] = Field(None, description="""related Entity - storing the complete Affiliation info etc.""")
    user: User = Field(..., description="""back-reference to the Django user""")
    home_screen_URL: Optional[str] = Field(None, description="""URL to the home screen - this is the default screen after login and can be changed by the user""")
    home_screen_layout: Optional[str] = Field(None, description="""layout = what to see on the home screen""")
    access_TOKEN: Optional[str] = Field(None, description="""LARA-django (API) access token""")
    access_control: Optional[str] = Field(None, description="""Access control, similar to Linux ACL""")
    confirmation_TOKEN: Optional[str] = Field(None, description="""LARA-django e-mail confirmation token""")
    email_confirmed: Optional[str] = Field(None, description="""LARA user confirmed e-mail""")
    email_recover: Optional[str] = Field(None, description="""LARA user account recovering e-mail""")
    max_logins: Optional[int] = Field(None, description="""max number of logins""")
    failed_logins: Optional[int] = Field(None, description="""number of failed login attempts""")
    IP_curr_login: Optional[str] = Field(None, description="""IP address of current  login""")
    IP_last_login: Optional[str] = Field(None, description="""IP address of last login""")
    datetime_confirmed: Optional[date] = Field(None, description="""datetime of confirmation""")
    datetime_confirmation_sent: Optional[date] = Field(None, description="""datetime of confirmation was sent to the user""")
    
        

class MeetingsCalendar(ConfiguredBaseModel):
    """
    \" Meeting Calendar \"
    """
    title: str = Field(..., description="""title of the calendar entry""")
    calendar_URL: Optional[str] = Field(None, description="""Universal Resource Locator (URL), to google calendar, iCalendar, .... """)
    summary: Optional[str] = Field(None, description="""short summary text of the calendar entry""")
    description: Optional[str] = Field(None, description="""text of the calendar entry""")
    color: Optional[str] = Field(None, description="""text of the calendar entry""")
    ics: Optional[str] = Field(None, description="""iCalendar entry in (RFC 5545) file format""")
    datetime_start: date = Field(..., description="""start date & time of the event. RFC 5545 - DTSTART""")
    datetime_end: date = Field(..., description="""end datetime of the event. RFC 5545 - DTEND""")
    duration: Optional[str] = Field(None, description=""" This value type is used to identify properties that contain a duration of time. RFC 5545 - DURATION""")
    all_day: bool = Field(..., description="""All day event ? (boolean)""")
    created: date = Field(..., description="""end datetime of the event""")
    datetime_last_modified: date = Field(..., description="""date and time when calendar entry was last modified""")
    timestamp: date = Field(..., description="""timestamp of the event""")
    location: Optional[Location] = Field(None, description="""location""")
    geolocation: Optional[GeoLocation] = Field(None, description="""GEO location""")
    conference_URL: Optional[str] = Field(None, description="""audio/video conference/meeting Universal Resource Locator (URL), e.g. zoom, jitsi, meet, ...""")
    range: Optional[str] = Field(None, description="""To specify the effective range of recurrence instances from the instance specified by the recurrence identifier specified by the property. RFC 5545 - RANGE """)
    related: Optional[str] = Field(None, description="""To specify the relationship of the alarm trigger with respect to the start or end of the calendar component. RFC 5545 - RELATED""")
    role: Optional[str] = Field(None, description="""To specify the participation role for the calendar user specified by the property. RFC 5545 - ROLE""")
    tzid: Optional[str] = Field(None, description="""To specify the identifier for the time zone definition for a time component in the property value. RFC 5545 - TZID""")
    offset_UTC: Optional[int] = Field(None, description=""" This value type is used to identify properties that contain an offset from UTC to local time (+ or - num. hours) . RFC 5545 -  UTC-OFFSET""")
    alarm_repeat_JSON: Optional[str] = Field(None, description="""advanced alarm repeat expressions possible, e.g. for uneven occurrences""")
    event_repeat: Optional[str] = Field(None, description="""apache airflow ('@daily', '@weekly') or cron expression like entry: '12 23 * * *' :  every day at 23:12h do this task""")
    event_repeat_JSON: Optional[str] = Field(None, description="""advanced event repeat expressions possible, e.g. for uneven occurrences""")
    calendar_id: Optional[str] = Field(None, description="""MeetingsCalendar - primary key (UUID)""")
    organiser: Optional[LaraUser] = Field(None, description="""LARA User, who created the calendar entry""")
    tags: Optional[List[Tag]] = Field(default_factory=list, description="""tags of calendar entry""")
    attendees: Optional[List[LaraUser]] = Field(default_factory=list, description="""attendees of the meeting""")
    
        

class Container(ConfiguredBaseModel):
    
    extradatas: Optional[List[ExtraData]] = Field(default_factory=list)
    entityclasss: Optional[List[EntityClass]] = Field(default_factory=list)
    entityroles: Optional[List[EntityRole]] = Field(default_factory=list)
    entitytitles: Optional[List[EntityTitle]] = Field(default_factory=list)
    entitys: Optional[List[Entity]] = Field(default_factory=list)
    groups: Optional[List[Group]] = Field(default_factory=list)
    entitybankaccounts: Optional[List[EntityBankAccount]] = Field(default_factory=list)
    larausers: Optional[List[LaraUser]] = Field(default_factory=list)
    meetingscalendars: Optional[List[MeetingsCalendar]] = Field(default_factory=list)
    
        


# Model rebuild
# see https://pydantic-docs.helpmanual.io/usage/models/#rebuilding-a-model
MediaType.model_rebuild()
Address.model_rebuild()
Tag.model_rebuild()
Location.model_rebuild()
Namespace.model_rebuild()
GeoLocation.model_rebuild()
User.model_rebuild()
Room.model_rebuild()
DataType.model_rebuild()
ExtraData.model_rebuild()
EntityClass.model_rebuild()
EntityRole.model_rebuild()
EntityTitle.model_rebuild()
Entity.model_rebuild()
Group.model_rebuild()
EntityBankAccount.model_rebuild()
LaraUser.model_rebuild()
MeetingsCalendar.model_rebuild()
Container.model_rebuild()

