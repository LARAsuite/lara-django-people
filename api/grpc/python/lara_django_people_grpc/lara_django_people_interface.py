"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_people high_level_interface *

:details: lara_django_people high_level_interface.
         - generated with 'lara-django-dev generate_high_level_interface lara_django_people
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

import json
import asyncio
import logging
import grpc

import lara_django_base_grpc.v1.lara_django_base_pb2 as lara_django_base_pb2
import lara_django_base_grpc.v1.lara_django_base_pb2_grpc as lara_django_base_pb2_grpc

import lara_django_people_grpc.v1.lara_django_people_pb2 as lara_django_people_pb2
import lara_django_people_grpc.v1.lara_django_people_pb2_grpc as lara_django_people_pb2_grpc

import  lara_django_people_grpc.lara_django_people_data_model as people_dm 



class ExtraDataInterface:
    def __init__(self, channel: grpc.Channel = None, default_namespace_URI : str = None) -> None:
        if default_namespace_URI is not None:
            self.default_namespace_URI = default_namespace_URI
        self.namespaceURI_client = (
            lara_django_base_pb2_grpc.NamespaceByURIControllerStub(channel)
        )
        # self.people_class_client = (
        #     lara_django_people_pb2_grpc.lara_django_peopleclassByIRIControllerStub(channel)
        # )
        
        self.extradata_client = lara_django_people_pb2_grpc.ExtradataControllerStub(channel)

        # self.people_full_client = lara_django_people_pb2_grpc.ExtradataFullControllerStub(
        #     channel
        # )

    def create(self,  extradata:  people_dm.ExtraData = None, namespace_URI: str = None):
        if namespace_URI is None and self.default_namespace_URI is not None:
            namespace_URI = self.default_namespace_URI
        if namespace_URI is None:
            raise ValueError("No namespace URI is given.")
        try:
            namespace_id = self.namespaceURI_client.Retrieve(
                lara_django_base_pb2.NamespaceRetrieveRequest(URI=namespace_URI)
            ).namespace_id
        except Exception as e:
            logging.error(f"Error retrieving namespace_id: {e}")
        
        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if extradata_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # extradata_class_id = self.extradata_class_client.Retrieve(
        #     lara_django_people_pb2.ExtradataclassRetrieveRequest(IRI=extradata_class_iri)
        # ).extradata_class_id
        # extradata.namespace = namespace_id
        # extradata.extradata_class = extradata_class_id

        return self.extradata_client.Create(lara_django_people_pb2.ExtradataRequest(**extradata.model_dump()))

    def retrieve(
        self, 
        extradata_id: str = None,
        extradata_name: str = None,
        #extradata_name_full: str = None,
        ):
        results_list = []
        if  extradata_id is not None:
            try:
                results_list.append( self.extradata_client.Retrieve(
                    lara_django_people_pb2.ExtradataRetrieveRequest(extradata_id=extradata_id) )
                )
            except Exception as e:
                logging.error(f"Error retrieving extradata_id: {e}")
        elif extradata_name is not None:
            filter_dict = {"name": extradata_name}
            metadata = (("filters", (json.dumps(filter_dict))),)
            try:
                results_list.append( self.extradata_client.List(
                    lara_django_people_pb2.ExtradataListRequest(), metadata=metadata
                ))
            except Exception as e:
                logging.error(f"Error retrieving extradata_name: {e}")
        # elif extradata_name_full is not None:
        #     filter_dict = {"name_full": extradata_name_full}
        #     metadata = (("filters", (json.dumps(filter_dict))),)
        #     try:
        #         results_list.append( self.extradata_client.List(
        #             lara_django_people_pb2.ExtradataListRequest(), metadata=metadata
        #         ))
        #     except Exception as e:
        #         logging.error(f"Error retrieving extradata_name_full: {e}")

        return results_list
        
    def list(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.extradata_client.List(lara_django_people_pb2.ExtradataListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.extradata_client.List(
                lara_django_people_pb2.ExtradataListRequest(), metadata=metadata
            )

    def list_full(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.extradata_full_client.List(
                lara_django_people_pb2.ExtradataFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.extradata_full_client.List(
                lara_django_people_pb2.ExtradataFullListRequest(), metadata=metadata
            )

    def update(self, extradata : people_dm.ExtraData = None):
        try:
            return self.extradata_client.Update(
                lara_django_people_pb2.ExtradataRequest(**extradata.model_dump()
            ))
        except Exception as e:
            logging.error(f"Error updating extradata_id: {e}")

    def delete(self, extradata_id: str = None):
        try:
            return self.extradata_client.Destroy(
                lara_django_people_pb2.ExtradataRequest(extradata_id=extradata_id)
            )
        except Exception as e:
            logging.error(f"Error deleting extradata_id: {e}")

class EntityClassInterface:
    def __init__(self, channel: grpc.Channel = None, default_namespace_URI : str = None) -> None:
        if default_namespace_URI is not None:
            self.default_namespace_URI = default_namespace_URI
        self.namespaceURI_client = (
            lara_django_base_pb2_grpc.NamespaceByURIControllerStub(channel)
        )
        # self.people_class_client = (
        #     lara_django_people_pb2_grpc.lara_django_peopleclassByIRIControllerStub(channel)
        # )
        
        self.entityclass_client = lara_django_people_pb2_grpc.EntityclassControllerStub(channel)

        # self.people_full_client = lara_django_people_pb2_grpc.EntityclassFullControllerStub(
        #     channel
        # )

    def create(self,  entityclass:  people_dm.EntityClass = None, namespace_URI: str = None):
        if namespace_URI is None and self.default_namespace_URI is not None:
            namespace_URI = self.default_namespace_URI
        if namespace_URI is None:
            raise ValueError("No namespace URI is given.")
        try:
            namespace_id = self.namespaceURI_client.Retrieve(
                lara_django_base_pb2.NamespaceRetrieveRequest(URI=namespace_URI)
            ).namespace_id
        except Exception as e:
            logging.error(f"Error retrieving namespace_id: {e}")
        
        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if entityclass_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # entityclass_class_id = self.entityclass_class_client.Retrieve(
        #     lara_django_people_pb2.EntityclassclassRetrieveRequest(IRI=entityclass_class_iri)
        # ).entityclass_class_id
        # entityclass.namespace = namespace_id
        # entityclass.entityclass_class = entityclass_class_id

        return self.entityclass_client.Create(lara_django_people_pb2.EntityclassRequest(**entityclass.model_dump()))

    def retrieve(
        self, 
        entityclass_id: str = None,
        entityclass_name: str = None,
        #entityclass_name_full: str = None,
        ):
        results_list = []
        if  entityclass_id is not None:
            try:
                results_list.append( self.entityclass_client.Retrieve(
                    lara_django_people_pb2.EntityclassRetrieveRequest(entityclass_id=entityclass_id) )
                )
            except Exception as e:
                logging.error(f"Error retrieving entityclass_id: {e}")
        elif entityclass_name is not None:
            filter_dict = {"name": entityclass_name}
            metadata = (("filters", (json.dumps(filter_dict))),)
            try:
                results_list.append( self.entityclass_client.List(
                    lara_django_people_pb2.EntityclassListRequest(), metadata=metadata
                ))
            except Exception as e:
                logging.error(f"Error retrieving entityclass_name: {e}")
        # elif entityclass_name_full is not None:
        #     filter_dict = {"name_full": entityclass_name_full}
        #     metadata = (("filters", (json.dumps(filter_dict))),)
        #     try:
        #         results_list.append( self.entityclass_client.List(
        #             lara_django_people_pb2.EntityclassListRequest(), metadata=metadata
        #         ))
        #     except Exception as e:
        #         logging.error(f"Error retrieving entityclass_name_full: {e}")

        return results_list
        
    def list(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.entityclass_client.List(lara_django_people_pb2.EntityclassListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.entityclass_client.List(
                lara_django_people_pb2.EntityclassListRequest(), metadata=metadata
            )

    def list_full(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.entityclass_full_client.List(
                lara_django_people_pb2.EntityclassFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.entityclass_full_client.List(
                lara_django_people_pb2.EntityclassFullListRequest(), metadata=metadata
            )

    def update(self, entityclass : people_dm.EntityClass = None):
        try:
            return self.entityclass_client.Update(
                lara_django_people_pb2.EntityclassRequest(**entityclass.model_dump()
            ))
        except Exception as e:
            logging.error(f"Error updating entityclass_id: {e}")

    def delete(self, entityclass_id: str = None):
        try:
            return self.entityclass_client.Destroy(
                lara_django_people_pb2.EntityclassRequest(entityclass_id=entityclass_id)
            )
        except Exception as e:
            logging.error(f"Error deleting entityclass_id: {e}")

class EntityRoleInterface:
    def __init__(self, channel: grpc.Channel = None, default_namespace_URI : str = None) -> None:
        if default_namespace_URI is not None:
            self.default_namespace_URI = default_namespace_URI
        self.namespaceURI_client = (
            lara_django_base_pb2_grpc.NamespaceByURIControllerStub(channel)
        )
        # self.people_class_client = (
        #     lara_django_people_pb2_grpc.lara_django_peopleclassByIRIControllerStub(channel)
        # )
        
        self.entityrole_client = lara_django_people_pb2_grpc.EntityroleControllerStub(channel)

        # self.people_full_client = lara_django_people_pb2_grpc.EntityroleFullControllerStub(
        #     channel
        # )

    def create(self,  entityrole:  people_dm.EntityRole = None, namespace_URI: str = None):
        if namespace_URI is None and self.default_namespace_URI is not None:
            namespace_URI = self.default_namespace_URI
        if namespace_URI is None:
            raise ValueError("No namespace URI is given.")
        try:
            namespace_id = self.namespaceURI_client.Retrieve(
                lara_django_base_pb2.NamespaceRetrieveRequest(URI=namespace_URI)
            ).namespace_id
        except Exception as e:
            logging.error(f"Error retrieving namespace_id: {e}")
        
        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if entityrole_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # entityrole_class_id = self.entityrole_class_client.Retrieve(
        #     lara_django_people_pb2.EntityroleclassRetrieveRequest(IRI=entityrole_class_iri)
        # ).entityrole_class_id
        # entityrole.namespace = namespace_id
        # entityrole.entityrole_class = entityrole_class_id

        return self.entityrole_client.Create(lara_django_people_pb2.EntityroleRequest(**entityrole.model_dump()))

    def retrieve(
        self, 
        entityrole_id: str = None,
        entityrole_name: str = None,
        #entityrole_name_full: str = None,
        ):
        results_list = []
        if  entityrole_id is not None:
            try:
                results_list.append( self.entityrole_client.Retrieve(
                    lara_django_people_pb2.EntityroleRetrieveRequest(entityrole_id=entityrole_id) )
                )
            except Exception as e:
                logging.error(f"Error retrieving entityrole_id: {e}")
        elif entityrole_name is not None:
            filter_dict = {"name": entityrole_name}
            metadata = (("filters", (json.dumps(filter_dict))),)
            try:
                results_list.append( self.entityrole_client.List(
                    lara_django_people_pb2.EntityroleListRequest(), metadata=metadata
                ))
            except Exception as e:
                logging.error(f"Error retrieving entityrole_name: {e}")
        # elif entityrole_name_full is not None:
        #     filter_dict = {"name_full": entityrole_name_full}
        #     metadata = (("filters", (json.dumps(filter_dict))),)
        #     try:
        #         results_list.append( self.entityrole_client.List(
        #             lara_django_people_pb2.EntityroleListRequest(), metadata=metadata
        #         ))
        #     except Exception as e:
        #         logging.error(f"Error retrieving entityrole_name_full: {e}")

        return results_list
        
    def list(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.entityrole_client.List(lara_django_people_pb2.EntityroleListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.entityrole_client.List(
                lara_django_people_pb2.EntityroleListRequest(), metadata=metadata
            )

    def list_full(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.entityrole_full_client.List(
                lara_django_people_pb2.EntityroleFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.entityrole_full_client.List(
                lara_django_people_pb2.EntityroleFullListRequest(), metadata=metadata
            )

    def update(self, entityrole : people_dm.EntityRole = None):
        try:
            return self.entityrole_client.Update(
                lara_django_people_pb2.EntityroleRequest(**entityrole.model_dump()
            ))
        except Exception as e:
            logging.error(f"Error updating entityrole_id: {e}")

    def delete(self, entityrole_id: str = None):
        try:
            return self.entityrole_client.Destroy(
                lara_django_people_pb2.EntityroleRequest(entityrole_id=entityrole_id)
            )
        except Exception as e:
            logging.error(f"Error deleting entityrole_id: {e}")

class EntityTitleInterface:
    def __init__(self, channel: grpc.Channel = None, default_namespace_URI : str = None) -> None:
        if default_namespace_URI is not None:
            self.default_namespace_URI = default_namespace_URI
        self.namespaceURI_client = (
            lara_django_base_pb2_grpc.NamespaceByURIControllerStub(channel)
        )
        # self.people_class_client = (
        #     lara_django_people_pb2_grpc.lara_django_peopleclassByIRIControllerStub(channel)
        # )
        
        self.entitytitle_client = lara_django_people_pb2_grpc.EntitytitleControllerStub(channel)

        # self.people_full_client = lara_django_people_pb2_grpc.EntitytitleFullControllerStub(
        #     channel
        # )

    def create(self,  entitytitle:  people_dm.EntityTitle = None, namespace_URI: str = None):
        if namespace_URI is None and self.default_namespace_URI is not None:
            namespace_URI = self.default_namespace_URI
        if namespace_URI is None:
            raise ValueError("No namespace URI is given.")
        try:
            namespace_id = self.namespaceURI_client.Retrieve(
                lara_django_base_pb2.NamespaceRetrieveRequest(URI=namespace_URI)
            ).namespace_id
        except Exception as e:
            logging.error(f"Error retrieving namespace_id: {e}")
        
        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if entitytitle_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # entitytitle_class_id = self.entitytitle_class_client.Retrieve(
        #     lara_django_people_pb2.EntitytitleclassRetrieveRequest(IRI=entitytitle_class_iri)
        # ).entitytitle_class_id
        # entitytitle.namespace = namespace_id
        # entitytitle.entitytitle_class = entitytitle_class_id

        return self.entitytitle_client.Create(lara_django_people_pb2.EntitytitleRequest(**entitytitle.model_dump()))

    def retrieve(
        self, 
        entitytitle_id: str = None,
        entitytitle_name: str = None,
        #entitytitle_name_full: str = None,
        ):
        results_list = []
        if  entitytitle_id is not None:
            try:
                results_list.append( self.entitytitle_client.Retrieve(
                    lara_django_people_pb2.EntitytitleRetrieveRequest(entitytitle_id=entitytitle_id) )
                )
            except Exception as e:
                logging.error(f"Error retrieving entitytitle_id: {e}")
        elif entitytitle_name is not None:
            filter_dict = {"name": entitytitle_name}
            metadata = (("filters", (json.dumps(filter_dict))),)
            try:
                results_list.append( self.entitytitle_client.List(
                    lara_django_people_pb2.EntitytitleListRequest(), metadata=metadata
                ))
            except Exception as e:
                logging.error(f"Error retrieving entitytitle_name: {e}")
        # elif entitytitle_name_full is not None:
        #     filter_dict = {"name_full": entitytitle_name_full}
        #     metadata = (("filters", (json.dumps(filter_dict))),)
        #     try:
        #         results_list.append( self.entitytitle_client.List(
        #             lara_django_people_pb2.EntitytitleListRequest(), metadata=metadata
        #         ))
        #     except Exception as e:
        #         logging.error(f"Error retrieving entitytitle_name_full: {e}")

        return results_list
        
    def list(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.entitytitle_client.List(lara_django_people_pb2.EntitytitleListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.entitytitle_client.List(
                lara_django_people_pb2.EntitytitleListRequest(), metadata=metadata
            )

    def list_full(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.entitytitle_full_client.List(
                lara_django_people_pb2.EntitytitleFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.entitytitle_full_client.List(
                lara_django_people_pb2.EntitytitleFullListRequest(), metadata=metadata
            )

    def update(self, entitytitle : people_dm.EntityTitle = None):
        try:
            return self.entitytitle_client.Update(
                lara_django_people_pb2.EntitytitleRequest(**entitytitle.model_dump()
            ))
        except Exception as e:
            logging.error(f"Error updating entitytitle_id: {e}")

    def delete(self, entitytitle_id: str = None):
        try:
            return self.entitytitle_client.Destroy(
                lara_django_people_pb2.EntitytitleRequest(entitytitle_id=entitytitle_id)
            )
        except Exception as e:
            logging.error(f"Error deleting entitytitle_id: {e}")

class EntityInterface:
    def __init__(self, channel: grpc.Channel = None, default_namespace_URI : str = None) -> None:
        if default_namespace_URI is not None:
            self.default_namespace_URI = default_namespace_URI
        self.namespaceURI_client = (
            lara_django_base_pb2_grpc.NamespaceByURIControllerStub(channel)
        )
        # self.people_class_client = (
        #     lara_django_people_pb2_grpc.lara_django_peopleclassByIRIControllerStub(channel)
        # )
        
        self.entity_client = lara_django_people_pb2_grpc.EntityControllerStub(channel)

        # self.people_full_client = lara_django_people_pb2_grpc.EntityFullControllerStub(
        #     channel
        # )

    def create(self,  entity:  people_dm.Entity = None, namespace_URI: str = None):
        if namespace_URI is None and self.default_namespace_URI is not None:
            namespace_URI = self.default_namespace_URI
        if namespace_URI is None:
            raise ValueError("No namespace URI is given.")
        try:
            namespace_id = self.namespaceURI_client.Retrieve(
                lara_django_base_pb2.NamespaceRetrieveRequest(URI=namespace_URI)
            ).namespace_id
        except Exception as e:
            logging.error(f"Error retrieving namespace_id: {e}")
        
        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if entity_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # entity_class_id = self.entity_class_client.Retrieve(
        #     lara_django_people_pb2.EntityclassRetrieveRequest(IRI=entity_class_iri)
        # ).entity_class_id
        # entity.namespace = namespace_id
        # entity.entity_class = entity_class_id

        return self.entity_client.Create(lara_django_people_pb2.EntityRequest(**entity.model_dump()))

    def retrieve(
        self, 
        entity_id: str = None,
        entity_name: str = None,
        #entity_name_full: str = None,
        ):
        results_list = []
        if  entity_id is not None:
            try:
                results_list.append( self.entity_client.Retrieve(
                    lara_django_people_pb2.EntityRetrieveRequest(entity_id=entity_id) )
                )
            except Exception as e:
                logging.error(f"Error retrieving entity_id: {e}")
        elif entity_name is not None:
            filter_dict = {"name": entity_name}
            metadata = (("filters", (json.dumps(filter_dict))),)
            try:
                results_list.append( self.entity_client.List(
                    lara_django_people_pb2.EntityListRequest(), metadata=metadata
                ))
            except Exception as e:
                logging.error(f"Error retrieving entity_name: {e}")
        # elif entity_name_full is not None:
        #     filter_dict = {"name_full": entity_name_full}
        #     metadata = (("filters", (json.dumps(filter_dict))),)
        #     try:
        #         results_list.append( self.entity_client.List(
        #             lara_django_people_pb2.EntityListRequest(), metadata=metadata
        #         ))
        #     except Exception as e:
        #         logging.error(f"Error retrieving entity_name_full: {e}")

        return results_list
        
    def list(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.entity_client.List(lara_django_people_pb2.EntityListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.entity_client.List(
                lara_django_people_pb2.EntityListRequest(), metadata=metadata
            )

    def list_full(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.entity_full_client.List(
                lara_django_people_pb2.EntityFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.entity_full_client.List(
                lara_django_people_pb2.EntityFullListRequest(), metadata=metadata
            )

    def update(self, entity : people_dm.Entity = None):
        try:
            return self.entity_client.Update(
                lara_django_people_pb2.EntityRequest(**entity.model_dump()
            ))
        except Exception as e:
            logging.error(f"Error updating entity_id: {e}")

    def delete(self, entity_id: str = None):
        try:
            return self.entity_client.Destroy(
                lara_django_people_pb2.EntityRequest(entity_id=entity_id)
            )
        except Exception as e:
            logging.error(f"Error deleting entity_id: {e}")

class GroupInterface:
    def __init__(self, channel: grpc.Channel = None, default_namespace_URI : str = None) -> None:
        if default_namespace_URI is not None:
            self.default_namespace_URI = default_namespace_URI
        self.namespaceURI_client = (
            lara_django_base_pb2_grpc.NamespaceByURIControllerStub(channel)
        )
        # self.people_class_client = (
        #     lara_django_people_pb2_grpc.lara_django_peopleclassByIRIControllerStub(channel)
        # )
        
        self.group_client = lara_django_people_pb2_grpc.GroupControllerStub(channel)

        # self.people_full_client = lara_django_people_pb2_grpc.GroupFullControllerStub(
        #     channel
        # )

    def create(self,  group:  people_dm.Group = None, namespace_URI: str = None):
        if namespace_URI is None and self.default_namespace_URI is not None:
            namespace_URI = self.default_namespace_URI
        if namespace_URI is None:
            raise ValueError("No namespace URI is given.")
        try:
            namespace_id = self.namespaceURI_client.Retrieve(
                lara_django_base_pb2.NamespaceRetrieveRequest(URI=namespace_URI)
            ).namespace_id
        except Exception as e:
            logging.error(f"Error retrieving namespace_id: {e}")
        
        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if group_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # group_class_id = self.group_class_client.Retrieve(
        #     lara_django_people_pb2.GroupclassRetrieveRequest(IRI=group_class_iri)
        # ).group_class_id
        # group.namespace = namespace_id
        # group.group_class = group_class_id

        return self.group_client.Create(lara_django_people_pb2.GroupRequest(**group.model_dump()))

    def retrieve(
        self, 
        group_id: str = None,
        group_name: str = None,
        #group_name_full: str = None,
        ):
        results_list = []
        if  group_id is not None:
            try:
                results_list.append( self.group_client.Retrieve(
                    lara_django_people_pb2.GroupRetrieveRequest(group_id=group_id) )
                )
            except Exception as e:
                logging.error(f"Error retrieving group_id: {e}")
        elif group_name is not None:
            filter_dict = {"name": group_name}
            metadata = (("filters", (json.dumps(filter_dict))),)
            try:
                results_list.append( self.group_client.List(
                    lara_django_people_pb2.GroupListRequest(), metadata=metadata
                ))
            except Exception as e:
                logging.error(f"Error retrieving group_name: {e}")
        # elif group_name_full is not None:
        #     filter_dict = {"name_full": group_name_full}
        #     metadata = (("filters", (json.dumps(filter_dict))),)
        #     try:
        #         results_list.append( self.group_client.List(
        #             lara_django_people_pb2.GroupListRequest(), metadata=metadata
        #         ))
        #     except Exception as e:
        #         logging.error(f"Error retrieving group_name_full: {e}")

        return results_list
        
    def list(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.group_client.List(lara_django_people_pb2.GroupListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.group_client.List(
                lara_django_people_pb2.GroupListRequest(), metadata=metadata
            )

    def list_full(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.group_full_client.List(
                lara_django_people_pb2.GroupFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.group_full_client.List(
                lara_django_people_pb2.GroupFullListRequest(), metadata=metadata
            )

    def update(self, group : people_dm.Group = None):
        try:
            return self.group_client.Update(
                lara_django_people_pb2.GroupRequest(**group.model_dump()
            ))
        except Exception as e:
            logging.error(f"Error updating group_id: {e}")

    def delete(self, group_id: str = None):
        try:
            return self.group_client.Destroy(
                lara_django_people_pb2.GroupRequest(group_id=group_id)
            )
        except Exception as e:
            logging.error(f"Error deleting group_id: {e}")

class EntityBankAccountInterface:
    def __init__(self, channel: grpc.Channel = None, default_namespace_URI : str = None) -> None:
        if default_namespace_URI is not None:
            self.default_namespace_URI = default_namespace_URI
        self.namespaceURI_client = (
            lara_django_base_pb2_grpc.NamespaceByURIControllerStub(channel)
        )
        # self.people_class_client = (
        #     lara_django_people_pb2_grpc.lara_django_peopleclassByIRIControllerStub(channel)
        # )
        
        self.entitybankaccount_client = lara_django_people_pb2_grpc.EntitybankaccountControllerStub(channel)

        # self.people_full_client = lara_django_people_pb2_grpc.EntitybankaccountFullControllerStub(
        #     channel
        # )

    def create(self,  entitybankaccount:  people_dm.EntityBankAccount = None, namespace_URI: str = None):
        if namespace_URI is None and self.default_namespace_URI is not None:
            namespace_URI = self.default_namespace_URI
        if namespace_URI is None:
            raise ValueError("No namespace URI is given.")
        try:
            namespace_id = self.namespaceURI_client.Retrieve(
                lara_django_base_pb2.NamespaceRetrieveRequest(URI=namespace_URI)
            ).namespace_id
        except Exception as e:
            logging.error(f"Error retrieving namespace_id: {e}")
        
        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if entitybankaccount_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # entitybankaccount_class_id = self.entitybankaccount_class_client.Retrieve(
        #     lara_django_people_pb2.EntitybankaccountclassRetrieveRequest(IRI=entitybankaccount_class_iri)
        # ).entitybankaccount_class_id
        # entitybankaccount.namespace = namespace_id
        # entitybankaccount.entitybankaccount_class = entitybankaccount_class_id

        return self.entitybankaccount_client.Create(lara_django_people_pb2.EntitybankaccountRequest(**entitybankaccount.model_dump()))

    def retrieve(
        self, 
        entitybankaccount_id: str = None,
        entitybankaccount_name: str = None,
        #entitybankaccount_name_full: str = None,
        ):
        results_list = []
        if  entitybankaccount_id is not None:
            try:
                results_list.append( self.entitybankaccount_client.Retrieve(
                    lara_django_people_pb2.EntitybankaccountRetrieveRequest(entitybankaccount_id=entitybankaccount_id) )
                )
            except Exception as e:
                logging.error(f"Error retrieving entitybankaccount_id: {e}")
        elif entitybankaccount_name is not None:
            filter_dict = {"name": entitybankaccount_name}
            metadata = (("filters", (json.dumps(filter_dict))),)
            try:
                results_list.append( self.entitybankaccount_client.List(
                    lara_django_people_pb2.EntitybankaccountListRequest(), metadata=metadata
                ))
            except Exception as e:
                logging.error(f"Error retrieving entitybankaccount_name: {e}")
        # elif entitybankaccount_name_full is not None:
        #     filter_dict = {"name_full": entitybankaccount_name_full}
        #     metadata = (("filters", (json.dumps(filter_dict))),)
        #     try:
        #         results_list.append( self.entitybankaccount_client.List(
        #             lara_django_people_pb2.EntitybankaccountListRequest(), metadata=metadata
        #         ))
        #     except Exception as e:
        #         logging.error(f"Error retrieving entitybankaccount_name_full: {e}")

        return results_list
        
    def list(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.entitybankaccount_client.List(lara_django_people_pb2.EntitybankaccountListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.entitybankaccount_client.List(
                lara_django_people_pb2.EntitybankaccountListRequest(), metadata=metadata
            )

    def list_full(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.entitybankaccount_full_client.List(
                lara_django_people_pb2.EntitybankaccountFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.entitybankaccount_full_client.List(
                lara_django_people_pb2.EntitybankaccountFullListRequest(), metadata=metadata
            )

    def update(self, entitybankaccount : people_dm.EntityBankAccount = None):
        try:
            return self.entitybankaccount_client.Update(
                lara_django_people_pb2.EntitybankaccountRequest(**entitybankaccount.model_dump()
            ))
        except Exception as e:
            logging.error(f"Error updating entitybankaccount_id: {e}")

    def delete(self, entitybankaccount_id: str = None):
        try:
            return self.entitybankaccount_client.Destroy(
                lara_django_people_pb2.EntitybankaccountRequest(entitybankaccount_id=entitybankaccount_id)
            )
        except Exception as e:
            logging.error(f"Error deleting entitybankaccount_id: {e}")

class LaraUserInterface:
    def __init__(self, channel: grpc.Channel = None, default_namespace_URI : str = None) -> None:
        if default_namespace_URI is not None:
            self.default_namespace_URI = default_namespace_URI
        self.namespaceURI_client = (
            lara_django_base_pb2_grpc.NamespaceByURIControllerStub(channel)
        )
        # self.people_class_client = (
        #     lara_django_people_pb2_grpc.lara_django_peopleclassByIRIControllerStub(channel)
        # )
        
        self.larauser_client = lara_django_people_pb2_grpc.LarauserControllerStub(channel)

        # self.people_full_client = lara_django_people_pb2_grpc.LarauserFullControllerStub(
        #     channel
        # )

    def create(self,  larauser:  people_dm.LaraUser = None, namespace_URI: str = None):
        if namespace_URI is None and self.default_namespace_URI is not None:
            namespace_URI = self.default_namespace_URI
        if namespace_URI is None:
            raise ValueError("No namespace URI is given.")
        try:
            namespace_id = self.namespaceURI_client.Retrieve(
                lara_django_base_pb2.NamespaceRetrieveRequest(URI=namespace_URI)
            ).namespace_id
        except Exception as e:
            logging.error(f"Error retrieving namespace_id: {e}")
        
        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if larauser_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # larauser_class_id = self.larauser_class_client.Retrieve(
        #     lara_django_people_pb2.LarauserclassRetrieveRequest(IRI=larauser_class_iri)
        # ).larauser_class_id
        # larauser.namespace = namespace_id
        # larauser.larauser_class = larauser_class_id

        return self.larauser_client.Create(lara_django_people_pb2.LarauserRequest(**larauser.model_dump()))

    def retrieve(
        self, 
        larauser_id: str = None,
        larauser_name: str = None,
        #larauser_name_full: str = None,
        ):
        results_list = []
        if  larauser_id is not None:
            try:
                results_list.append( self.larauser_client.Retrieve(
                    lara_django_people_pb2.LarauserRetrieveRequest(larauser_id=larauser_id) )
                )
            except Exception as e:
                logging.error(f"Error retrieving larauser_id: {e}")
        elif larauser_name is not None:
            filter_dict = {"name": larauser_name}
            metadata = (("filters", (json.dumps(filter_dict))),)
            try:
                results_list.append( self.larauser_client.List(
                    lara_django_people_pb2.LarauserListRequest(), metadata=metadata
                ))
            except Exception as e:
                logging.error(f"Error retrieving larauser_name: {e}")
        # elif larauser_name_full is not None:
        #     filter_dict = {"name_full": larauser_name_full}
        #     metadata = (("filters", (json.dumps(filter_dict))),)
        #     try:
        #         results_list.append( self.larauser_client.List(
        #             lara_django_people_pb2.LarauserListRequest(), metadata=metadata
        #         ))
        #     except Exception as e:
        #         logging.error(f"Error retrieving larauser_name_full: {e}")

        return results_list
        
    def list(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.larauser_client.List(lara_django_people_pb2.LarauserListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.larauser_client.List(
                lara_django_people_pb2.LarauserListRequest(), metadata=metadata
            )

    def list_full(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.larauser_full_client.List(
                lara_django_people_pb2.LarauserFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.larauser_full_client.List(
                lara_django_people_pb2.LarauserFullListRequest(), metadata=metadata
            )

    def update(self, larauser : people_dm.LaraUser = None):
        try:
            return self.larauser_client.Update(
                lara_django_people_pb2.LarauserRequest(**larauser.model_dump()
            ))
        except Exception as e:
            logging.error(f"Error updating larauser_id: {e}")

    def delete(self, larauser_id: str = None):
        try:
            return self.larauser_client.Destroy(
                lara_django_people_pb2.LarauserRequest(larauser_id=larauser_id)
            )
        except Exception as e:
            logging.error(f"Error deleting larauser_id: {e}")

class MeetingsCalendarInterface:
    def __init__(self, channel: grpc.Channel = None, default_namespace_URI : str = None) -> None:
        if default_namespace_URI is not None:
            self.default_namespace_URI = default_namespace_URI
        self.namespaceURI_client = (
            lara_django_base_pb2_grpc.NamespaceByURIControllerStub(channel)
        )
        # self.people_class_client = (
        #     lara_django_people_pb2_grpc.lara_django_peopleclassByIRIControllerStub(channel)
        # )
        
        self.meetingscalendar_client = lara_django_people_pb2_grpc.MeetingscalendarControllerStub(channel)

        # self.people_full_client = lara_django_people_pb2_grpc.MeetingscalendarFullControllerStub(
        #     channel
        # )

    def create(self,  meetingscalendar:  people_dm.MeetingsCalendar = None, namespace_URI: str = None):
        if namespace_URI is None and self.default_namespace_URI is not None:
            namespace_URI = self.default_namespace_URI
        if namespace_URI is None:
            raise ValueError("No namespace URI is given.")
        try:
            namespace_id = self.namespaceURI_client.Retrieve(
                lara_django_base_pb2.NamespaceRetrieveRequest(URI=namespace_URI)
            ).namespace_id
        except Exception as e:
            logging.error(f"Error retrieving namespace_id: {e}")
        
        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if meetingscalendar_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # meetingscalendar_class_id = self.meetingscalendar_class_client.Retrieve(
        #     lara_django_people_pb2.MeetingscalendarclassRetrieveRequest(IRI=meetingscalendar_class_iri)
        # ).meetingscalendar_class_id
        # meetingscalendar.namespace = namespace_id
        # meetingscalendar.meetingscalendar_class = meetingscalendar_class_id

        return self.meetingscalendar_client.Create(lara_django_people_pb2.MeetingscalendarRequest(**meetingscalendar.model_dump()))

    def retrieve(
        self, 
        meetingscalendar_id: str = None,
        meetingscalendar_name: str = None,
        #meetingscalendar_name_full: str = None,
        ):
        results_list = []
        if  meetingscalendar_id is not None:
            try:
                results_list.append( self.meetingscalendar_client.Retrieve(
                    lara_django_people_pb2.MeetingscalendarRetrieveRequest(meetingscalendar_id=meetingscalendar_id) )
                )
            except Exception as e:
                logging.error(f"Error retrieving meetingscalendar_id: {e}")
        elif meetingscalendar_name is not None:
            filter_dict = {"name": meetingscalendar_name}
            metadata = (("filters", (json.dumps(filter_dict))),)
            try:
                results_list.append( self.meetingscalendar_client.List(
                    lara_django_people_pb2.MeetingscalendarListRequest(), metadata=metadata
                ))
            except Exception as e:
                logging.error(f"Error retrieving meetingscalendar_name: {e}")
        # elif meetingscalendar_name_full is not None:
        #     filter_dict = {"name_full": meetingscalendar_name_full}
        #     metadata = (("filters", (json.dumps(filter_dict))),)
        #     try:
        #         results_list.append( self.meetingscalendar_client.List(
        #             lara_django_people_pb2.MeetingscalendarListRequest(), metadata=metadata
        #         ))
        #     except Exception as e:
        #         logging.error(f"Error retrieving meetingscalendar_name_full: {e}")

        return results_list
        
    def list(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.meetingscalendar_client.List(lara_django_people_pb2.MeetingscalendarListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.meetingscalendar_client.List(
                lara_django_people_pb2.MeetingscalendarListRequest(), metadata=metadata
            )

    def list_full(self, filter_dict: dict = None):
        if filter_dict is None:
            return self.meetingscalendar_full_client.List(
                lara_django_people_pb2.MeetingscalendarFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return self.meetingscalendar_full_client.List(
                lara_django_people_pb2.MeetingscalendarFullListRequest(), metadata=metadata
            )

    def update(self, meetingscalendar : people_dm.MeetingsCalendar = None):
        try:
            return self.meetingscalendar_client.Update(
                lara_django_people_pb2.MeetingscalendarRequest(**meetingscalendar.model_dump()
            ))
        except Exception as e:
            logging.error(f"Error updating meetingscalendar_id: {e}")

    def delete(self, meetingscalendar_id: str = None):
        try:
            return self.meetingscalendar_client.Destroy(
                lara_django_people_pb2.MeetingscalendarRequest(meetingscalendar_id=meetingscalendar_id)
            )
        except Exception as e:
            logging.error(f"Error deleting meetingscalendar_id: {e}")

